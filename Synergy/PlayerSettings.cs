﻿using System;
using System.IO;
using System.Numerics;
using Synergy.Terrain;

namespace Synergy
{
    public class PlayerSettings
    {
        public const Int32 HotbarCount = 7;
        public const Int32 HotbarWidth = 10;

        private const Int32 DataBlockCount = 3;
        private const Int32 WorldPositionBlockIdentifier = 0;
        private const Int32 HotbarBlockIdentifier = 1;
        private const Int32 SurivalHotbarBlockIdentifier = 2;

        public CubeCoords WorldPosition { get; set; }

        public Vector3 Forward { get; set; }

        public Vector3 Look { get; set; }

        public Int32 CurrentWeaponType { get; set; }

        public Cube[,] HotbarCubes { get; set; }
        public Cube[,] SurvivalHotbarCubes { get; set; }
        public Int32[,] SurvivalHotbarItemIds { get; set; }

        public PlayerSettings()
        {
            HotbarCubes = new Cube[HotbarCount, HotbarWidth];
            SurvivalHotbarCubes = new Cube[HotbarCount, HotbarWidth];
            SurvivalHotbarItemIds = new Int32[HotbarCount, HotbarWidth];
        }

        public void LoadSettings(Stream stream)
        {
            var reader = new BinaryReader(stream);
            Int32 version = reader.ReadInt32();
            switch (version)
            {
                case 0:
                    Int32 dataBlockCount = reader.ReadInt32();
                    while (reader.BaseStream.Position != reader.BaseStream.Length)
                    {
                        Int32 dataBlockType = reader.ReadInt32();
                        switch (dataBlockType)
                        {
                            case WorldPositionBlockIdentifier:
                                LoadWorldPosition(reader);
                                break;
                            case HotbarBlockIdentifier:
                                LoadHotbar(reader);
                                break;
                            case SurivalHotbarBlockIdentifier:
                                LoadSurvivalHotbar(reader);
                                break;
                            default:
                                throw new Exception("File format (data block " + version + ") of " + Player.SettingsFileName + " is not recognized!");
                        }
                    }
                    break;
                default:
                    throw new Exception("File format (version " + version + ") of " + Player.SettingsFileName + " is not recognized!");
            }
        }
        public void WriteSettings(Stream stream, Int32 version)
        {
            var writer = new BinaryWriter(stream);
            writer.Write(version);
            switch (version)
            {
                case 0:
                    writer.Write(DataBlockCount);
                    WriteWorldPosition(writer, 1);
                    WriteHotbar(writer, 0);
                    WriteSurvivalHotbar(writer, 0);
                    break;
                default:
                    throw new Exception("File format (version " + version + ") of " + Player.SettingsFileName + " is not recognized!");
            }
        }

        private void LoadWorldPosition(BinaryReader reader)
        {
            Int32 version = reader.ReadInt32();
            switch (version)
            {
                case 1:
                    WorldPosition = new CubeCoords(reader.ReadInt64(), reader.ReadInt64(), reader.ReadInt64());
                    Forward = new Vector3(reader.ReadSingle(), reader.ReadSingle(), reader.ReadSingle());
                    Look = new Vector3(reader.ReadSingle(), reader.ReadSingle(), reader.ReadSingle());
                    CurrentWeaponType = reader.ReadInt32();
                    break;
                default:
                    throw new Exception("Data block format (version " + version + ") of " + Player.SettingsFileName + " is not recognized!");
            }
        }
        private void LoadHotbar(BinaryReader reader)
        {
            Int32 version = reader.ReadInt32();
            switch (version)
            {
                case 0:
                    for (Int32 i = 0; i < HotbarCount; i++)
                    {
                        for (Int32 j = 0; j < HotbarWidth; j++)
                        {
                            HotbarCubes[i, j] = new Cube(reader.ReadUInt16(), 0, reader.ReadUInt16(), 0);
                        }
                    }
                    break;
                default:
                    throw new Exception("Data block format (version " + version + ") of " + Player.SettingsFileName + " is not recognized!");
            }
        }
        private void LoadSurvivalHotbar(BinaryReader reader)
        {
            Int32 version = reader.ReadInt32();
            switch (version)
            {
                case 0:
                    for (Int32 i = 0; i < HotbarCount; i++)
                    {
                        for (Int32 j = 0; j < HotbarWidth; j++)
                        {
                            SurvivalHotbarItemIds[i, j] = reader.ReadInt32();
                            SurvivalHotbarCubes[i, j] = new Cube(reader.ReadUInt16(), 0, reader.ReadUInt16(), 0);
                        }
                    }
                    break;
                default:
                    throw new Exception("Data block format (version " + version + ") of " + Player.SettingsFileName + " is not recognized!");
            }
        }

        private void WriteWorldPosition(BinaryWriter writer, Int32 version)
        {
            writer.Write(WorldPositionBlockIdentifier);
            writer.Write(version);
            switch (version)
            {
                case 1:
                    writer.Write(WorldPosition.X);
                    writer.Write(WorldPosition.Y);
                    writer.Write(WorldPosition.Z);
                    writer.Write(Forward.X);
                    writer.Write(Forward.Y);
                    writer.Write(Forward.Z);
                    writer.Write(Look.X);
                    writer.Write(Look.Y);
                    writer.Write(Look.Z);
                    writer.Write(CurrentWeaponType);
                    break;
                default:
                    throw new Exception("Data block format (version " + version + ") of " + Player.SettingsFileName + " is not recognized!");
            }
        }
        private void WriteHotbar(BinaryWriter writer, Int32 version)
        {
            writer.Write(HotbarBlockIdentifier);
            writer.Write(version);
            switch (version)
            {
                case 0:
                    for (Int32 i = 0; i < HotbarCount; i++)
                    {
                        for (Int32 j = 0; j < HotbarWidth; j++)
                        {
                            writer.Write(HotbarCubes[i, j].TypeId);
                            writer.Write(HotbarCubes[i, j].Extra);
                        }
                    }
                    break;
                default:
                    throw new Exception("Data block format (version " + version + ") of " + Player.SettingsFileName + " is not recognized!");
            }
        }
        private void WriteSurvivalHotbar(BinaryWriter writer, Int32 version)
        {
            writer.Write(SurivalHotbarBlockIdentifier);
            writer.Write(version);
            switch (version)
            {
                case 0:
                    for (Int32 i = 0; i < HotbarCount; i++)
                    {
                        for (Int32 j = 0; j < HotbarWidth; j++)
                        {
                            writer.Write(SurvivalHotbarItemIds[i, j]);
                            writer.Write(SurvivalHotbarCubes[i, j].TypeId);
                            writer.Write(SurvivalHotbarCubes[i, j].Extra);
                        }
                    }
                    break;
                default:
                    throw new Exception("Data block format (version " + version + ") of " + Player.SettingsFileName + " is not recognized!");
            }
        }
    }
}
