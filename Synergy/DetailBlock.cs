﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Synergy.Terrain;

namespace Synergy
{
    public class DetailBlock
    {
        public String Name { get; set; }
        public String CreatorTag { get; set; }
        public Int32 UsageState { get; set; }
        public Int32 DimensionLength { get; set; }
        public Byte Flags { get; set; }
        public Cube[] Cubes { get; set; }
        public List<DetailBlockBehavior> Behaviors { get; set; }

        public const String DetailBlockSlotFileName = "detailBlockSlots.dat";
        public const String WorldDetailBlockDirectory = "Detail Blocks";

        public DetailBlock()
        {
            Behaviors = new List<DetailBlockBehavior>();
        }

        public void Load(BinaryReader reader)
        {
            Int32 num = reader.ReadInt32();
            if (num != 1)
            {
                throw new Exception("Custom object file version mismatch!");
            }
            Name = reader.ReadString();
            CreatorTag = reader.ReadString();
            UsageState = reader.ReadInt32();
            DimensionLength = reader.ReadInt32();
            Cubes = new Cube[DimensionLength * DimensionLength * DimensionLength];
            Int32 byteCount = reader.ReadInt32();
            Int64 beginOffset = reader.BaseStream.Position;
            Int32 xCoord = 0;
            Int32 zCoord = 0;
            Int32 yCoord = 0;
            while (reader.BaseStream.Position < beginOffset + byteCount)
            {
                UInt16 cubeType = reader.ReadUInt16();
                Byte flags = reader.ReadByte();
                Int32 runLength = 1;
                UInt16 extraData;
                if (cubeType == UInt16.MaxValue)
                {
                    runLength = (Int32)(flags + 2);
                    cubeType = reader.ReadUInt16();
                    flags = reader.ReadByte();
                    extraData = reader.ReadUInt16();
                }
                else
                {
                    extraData = reader.ReadUInt16();
                }
                if (cubeType == UInt16.MaxValue)
                {
                    throw new Exception("Corrupted file - found cube type MAX");
                }
                for (Int32 i = 0; i < runLength; i++)
                {
                    Int32 index = yCoord * DimensionLength * DimensionLength + zCoord * DimensionLength + xCoord;
                    var cube = new Cube {TypeId = cubeType, Flags = flags, Extra = extraData};
                    Cubes[index] = cube;
                    xCoord++;
                    if (xCoord == DimensionLength)
                    {
                        xCoord = 0;
                        zCoord++;
                        if (zCoord == DimensionLength)
                        {
                            zCoord = 0;
                            yCoord++;
                            if (yCoord >= DimensionLength)
                            {
                            }
                        }
                    }
                }
            }
            if (reader.BaseStream.Position < beginOffset + (Int64)byteCount)
            {
                throw new Exception("Disk: Failed to read all block data from serialised custom: " + Name);
            }
            Int32 behaviorCount = reader.ReadInt32();
            Behaviors.Clear();
            for (Int32 j = 0; j < behaviorCount; j++)
            {
                Behaviors.Add(new DetailBlockBehavior
                    {
                        BlockBehavior = (BlockBehavior)reader.ReadInt32(),
                        Size = reader.ReadSingle(),
                        Speed = reader.ReadSingle()
                    });
            }
        }

        public static String[] ReadSlots(BinaryReader reader)
        {
            String[] buildableSlotFiles = new String[0x4001];
            Int32 version = reader.ReadInt32();
            if (version != 1) throw new Exception("Version Mismatch!");
            Int32 buildableSlotCount = reader.ReadInt32();
            if (buildableSlotCount != buildableSlotFiles.Length)
            {
                throw new Exception("Slot file count mismatch");
            }
            
            for (Int32 i = 0; i < buildableSlotCount; i++)
            {
                String slotFile = reader.ReadString();
                if (slotFile != String.Empty)
                {
                    buildableSlotFiles[i] = slotFile;
                }
            }

            // Since non buildable slots don't appear to be used, we'll just finish reading them in but do nothing with them.
            Int32 nonbuildableSlotCount = reader.ReadInt32();
            for (Int32 i = 0; i < nonbuildableSlotCount; i++)
            {
                String slotFile = reader.ReadString();
            }
            return buildableSlotFiles;
        }

        public static DetailBlock[] ReadDetailBlocks(String directory)
        {
            String[] blockFileNames = Directory.GetFiles(directory).Where(name => Path.GetExtension(name) == ".dat").ToArray();
            DetailBlock[] detailBlocks = new DetailBlock[blockFileNames.Length];
            for (Int32 i = 0; i < blockFileNames.Length; i++)
            {
                var block = new DetailBlock();
                using (var fs = File.Open(blockFileNames[i], FileMode.Open))
                {
                    var reader = new BinaryReader(fs);
                    block.Load(reader);
                }
                detailBlocks[i] = block;
            }
            return detailBlocks;
        }
    }
}