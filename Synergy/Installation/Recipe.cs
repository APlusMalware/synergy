﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using Synergy.Xml;

namespace Synergy.Installation
{
    [XmlElementName("CraftData")]
    public class Recipe
    {
        [XmlElementName]
        public String Key { get; set; }
        [XmlElementName]
        public String Category { get; set; }
        [XmlElementName]
        public UInt16? Tier { get; set; }
        [XmlElementName]
        public String CraftedName { get; set; }
        [XmlElementName]
        public UInt16? CraftedAmount { get; set; }
        [XmlElementName("Costs")]
        [XmlCollectionInnerName("CraftCost")]
        public List<CraftCost> Costs { get; set; }
        [XmlElementName]
        public UInt16? ResearchCost { get; set; }
        [XmlElementName]
        public String Description { get; set; }
        [XmlElementName]
        public String Hint { get; set; }
        [XmlElementName("ResearchRequirements")]
        [XmlCollectionInnerName("Research")]
        public List<String> ResearchRequirements { get; set; }
        [XmlElementName("ScanRequirements")]
        [XmlCollectionInnerName("Scan")]
        public List<ScanRequirement> ScanRequirements { get; set; }
        [XmlElementName]
        public Boolean? CanCraftAnywhere { get; set; }

        public static Dictionary<String, List<Recipe>> CreateDictionary(IEnumerable<Recipe> recipees)
        {
            var dictionary = new Dictionary<String, List<Recipe>>();
            foreach (Recipe recipe in recipees)
            {
                if (dictionary.ContainsKey(recipe.Key))
                    dictionary[recipe.Key].Add(recipe);
                else
                    dictionary.Add(recipe.Key, new List<Recipe> { recipe });
            }
            return dictionary;
        }

        public override String ToString()
        {
            return CraftedName;
        }
    }

    public class CraftCost
    {
        [XmlElementName]
        public String Name { get; set; }
        [XmlElementName]
        public Int32? Amount { get; set; }
    }
}
