﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using Synergy.Xml;

namespace Synergy.Installation
{
    [XmlElementName("ResearchDataEntry")]
    public class Research
    {
        [XmlElementName]
        public String Key { get; set; }
        [XmlElementName]
        public String Name { get; set; }
        [XmlElementName]
        public String IconName { get; set; }
        [XmlElementName]
        public UInt16? ResearchCost { get; set; }
        [XmlElementName]
        public String PreDescription { get; set; }
        [XmlElementName]
        public String PostDescription { get; set; }
        [XmlElementName("ResearchRequirements")]
        [XmlCollectionInnerName("Research")]
        public List<String> ResearchRequirements { get; set; }
        [XmlElementName("ProjectItemRequirements")]
        [XmlCollectionInnerName("Requirement")]
        public List<ProjectItemRequirement> ProjectRequirements { get; set; }
        [XmlElementName("ScanRequirements")]
        [XmlCollectionInnerName("Scan")]
        public List<ScanRequirement> ScanRequirements { get; set; }

        public static Dictionary<String, Research> CreateDictionary(IEnumerable<Research> researches)
        {
            var dictionary = new Dictionary<String, Research>();
            foreach(Research research in researches)
                dictionary.Add(research.Key, research);
            return dictionary;
        }

        public override String ToString()
        {
            return Name;
        }
    }

    [XmlElementName("Requirement")]
    public class ProjectItemRequirement
    {
        [XmlElementName]
        public String Name { get; set; }
        [XmlElementName]
        public UInt16? Amount { get; set; }
    }
    public class ScanRequirement
    {
        [XmlElementName("type")]
        public UInt16? Type { get; set; }
        [XmlElementName("value")]
        public Int32? Value { get; set; }
    }
}
