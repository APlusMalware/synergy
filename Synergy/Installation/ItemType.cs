﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using Synergy.Xml;

namespace Synergy.Installation
{
    public enum ItemCategory
    {
        Single,
        Stack,
        CubeStack,
        Durability,
        Charge,
        Location,
        Unknown
    }
    [XmlElementName("ItemEntry")]
    public class ItemType
    {
        [XmlElementName("ItemID")]
        public Int32 ItemId { get; set; }
        [XmlElementName]
        public String Name { get; set; }
        [XmlElementName]
        public String Plural { get; set; }
        [XmlElementName]
        public String Type { get; set; }
        [XmlElementName]
        public Boolean? Hidden { get; set; }
        [XmlElementName("Object")]
        public String ObjectDropped { get; set; }
        [XmlElementName]
        public String Sprite { get; set; }
        [XmlElementName]
        public String Category { get; set; }
        [XmlElementName]
        public String ItemAction { get; set; }
        [XmlElementName]
        public String ActionParameter { get; set; }

        public ItemCategory ItemCategory
        {
            get
            {
                switch (Type)
                {
                    case "ItemSingle":
                        return ItemCategory.Single;
                    case "ItemStack":
                        return ItemCategory.Stack;
                    case "ItemDurability":
                        return ItemCategory.Durability;
                    case "ItemCharge":
                        return ItemCategory.Charge;
                    case "ItemLocation":
                        return ItemCategory.Location;
                    default:
                        return ItemCategory.Unknown;
                }
            }
            set
            {
                switch (value)
                {
                    case ItemCategory.Single:
                        Type = "ItemSingle";
                        break;
                    case ItemCategory.Stack:
                        Type = "ItemStack";
                        break;
                    case ItemCategory.Durability:
                        Type = "ItemDurability";
                        break;
                    case ItemCategory.Charge:
                        Type = "ItemCharge";
                        break;
                    case ItemCategory.Location:
                        Type = "ItemLocation";
                        break;
                    default:
                        throw new Exception("ItemCategory " + value.ToString() + " is not writeable");
                }
            }
        }

        public static Dictionary<Int32, ItemType> CreateDictionary(IEnumerable<ItemType> items)
        {
            var dictionary = new Dictionary<Int32, ItemType>();
            foreach (ItemType item in items)
                dictionary.Add(item.ItemId, item);
            return dictionary;
        }

        public override String ToString()
        {
            return Name;
        }
    }
}
