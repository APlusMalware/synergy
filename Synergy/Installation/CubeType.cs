﻿using System;
using System.Collections.Generic;
using Synergy.Xml;

namespace Synergy.Installation
{
    [XmlElementName("TerrainDataEntry")]
    public class CubeType
    {
        [XmlElementName("CubeType")]
        public UInt16 TypeId { get; set; }
        [XmlElementName]
        public String Name { get; set; }
        [XmlElementName]
        public String Description { get; set; }
        [XmlElementName]
        public UInt16? ResearchValue { get; set; }
        [XmlElementName]
        public UInt16? Hardness { get; set; }

        [XmlElementName("Values")]
        [XmlCollectionInnerName("ValueEntry")]
		public List<ValueEntry> Values { get; set; }
        [XmlElementName]
        public UInt16? DefaultValue { get; set; }
        [XmlElementName]
        public String LayerType { get; set; }

        [XmlElementName]
        public UInt16 TopTexture { get; set; }
        [XmlElementName]
        public UInt16 SideTexture { get; set; }
        [XmlElementName]
        public UInt16 BottomTexture { get; set; }
        [XmlElementName("GUITexture")]
        public UInt16 GuiTexture { get; set; }

        [XmlElementName("Stages")]
        [XmlCollectionInnerName("Stage")]
        public List<Stage> Stages { get; set; }

        public Boolean? Hidden { get; set; }
        [XmlElementName("isSolid")]
        public Boolean IsSolid { get; set; }
        [XmlElementName("isTransparent")]
        public Boolean IsTransparent { get; set; }
        [XmlElementName("isHollow")]
        public Boolean IsHollow { get; set; }
        [XmlElementName("isGlass")]
        public Boolean IsGlass { get; set; }
        [XmlElementName("isPassable")]
        public Boolean IsPassable { get; set; }
        [XmlElementName("isColorised")]
        public Boolean? IsColorised { get; set; }
        [XmlElementName("isPaintable")]
        public Boolean? IsPaintable { get; set; }
        [XmlElementName("hasObject")]
        public Boolean HasObject { get; set; }
        [XmlElementName("hasEntity")]
        public Boolean HasEntity { get; set; }
        
        public Boolean IsOpen { get; set; }
        [XmlElementName]
        public Boolean IsAir { get; set; }

        [XmlElementName]
        public String Category { get; set; }

        [XmlElementName]
        public String AudioWalkType { get; set; }
        [XmlElementName]
        public String AudioBuildType { get; set; }
        [XmlElementName]
        public String AudioDestroyType { get; set; }

        [XmlElementName("tags")]
        [XmlCollectionInnerName("tag")]
        public List<String> Tags { get; set; }

        public const UInt16 MaxCubeTypeId = 0xFF00;
		public const UInt16 MinDetailTypeId = 0x7F00;
		public const UInt16 MaxDetailTypeId = 0xBF00;
        
        public static Dictionary<UInt16, CubeType> CreateDictionary(IEnumerable<CubeType> cubeTypes)
        {
            var dictionary = new Dictionary<UInt16, CubeType>();
            foreach (CubeType cubeType in cubeTypes)
                dictionary.Add(cubeType.TypeId, cubeType);
            return dictionary;
        }

        public override String ToString()
        {
            return Name;
        }
    }

	public class ValueEntry
	{
        [XmlElementName]
        public UInt16 Value { get; set; }
        [XmlElementName]
        public String Name { get; set; }
        [XmlElementName]
        public String IconName { get; set; }
        [XmlElementName]
        public String Description { get; set; }
        [XmlElementName]
        public UInt16? ResearchValue { get; set; }
	}

    public class Stage
    {
        [XmlElementName]
        public UInt16 RangeMinimum { get; set; }
        [XmlElementName]
        public UInt16 RangeMaximum { get; set; }
        [XmlElementName]
        public UInt16 TopTexture { get; set; }
        [XmlElementName]
        public UInt16 SideTexture { get; set; }
        [XmlElementName]
        public UInt16 BottomTexture { get; set; }
        [XmlElementName("GUITexture")]
        public UInt16 GuiTexture { get; set; }
    }
}
