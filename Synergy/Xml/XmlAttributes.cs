﻿using System;

namespace Synergy.Xml
{
    public class XmlElementName : Attribute
    {
        public String Name { get; }

        public XmlElementName()
        {
            Name = null;
        }
        public XmlElementName(String name)
        {
            Name = name;
        }
    }

    public class XmlCollectionInnerName : Attribute
    {
        public String Name { get; }

        public XmlCollectionInnerName(String name)
        {
            Name = name;
        }
    }
}