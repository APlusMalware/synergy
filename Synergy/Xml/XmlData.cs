﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Xml;

namespace Synergy.Xml
{
    public class XmlData
    {
        private readonly XmlDocument _xmlDocument;

        public XmlData(String fileName)
        {
            _xmlDocument = new XmlDocument();
            _xmlDocument.Load(fileName);
        }

        public T ReadXmlToType<T>()
        {
            Type type;
            Boolean isCollection = typeof(T).GetInterfaces().Any(p => p.GetGenericTypeDefinition() == typeof(ICollection<>));
            type = isCollection ? typeof(T).GetGenericArguments()[0] : type = typeof(T);
            String typeElementName = ((XmlElementName) type.GetCustomAttribute(typeof (XmlElementName)))?.Name;
            XmlProperties xmlProperties = ReadTypes(type);
            XmlNodeList typeElements = _xmlDocument.SelectNodes("*/" + typeElementName);

            if (!isCollection)
                return (T) ReadNode(typeElements[0], type, xmlProperties);
            
            MethodInfo addMethod = typeof(T).GetMethod("Add", new Type[] { type });

            T result = (T) typeof(T).GetConstructor(new Type[0]).Invoke(null);
            
            for (Int32 i = 0; i < typeElements.Count; i++)
            {
                XmlNode typeElement = typeElements.Item(i);
                addMethod.Invoke(result, new Object[] { ReadNode(typeElement, type, xmlProperties) });
            }

            return result;
        }

        private static Object ReadNode(XmlNode node, Type type, XmlProperties xmlProperties)
        {
            if (type == typeof (String) || type.IsPrimitive)
                return Convert.ChangeType(node.InnerText, type);
            if (type.IsGenericType && type.GetGenericTypeDefinition() == typeof (Nullable<>))
            {
                Type nullableType = Nullable.GetUnderlyingType(type);
                if (nullableType.IsPrimitive || nullableType == typeof (String))
                {
                    return Convert.ChangeType(node.InnerText, nullableType);
                }
            }

            ConstructorInfo constructor = type.GetConstructor(new Type[0]);

            Object obj = constructor.Invoke(null);
            foreach (PropertyInfo property in xmlProperties.PrimitiveProperties)
            {
                String elementName = xmlProperties.PropertyElementNames[property];
                XmlNode element = node.SelectSingleNode(elementName);
                if (element == null)
                    continue;
                
                if (property.PropertyType.IsPrimitive)
                    property.SetValue(obj, Convert.ChangeType(element.InnerText, property.PropertyType));
                else if (property.PropertyType == typeof(String))
                    property.SetValue(obj, element.InnerText);
                else
                    throw new Exception("Invalid property type");
            }
            foreach (KeyValuePair<PropertyInfo, XmlProperties> propertyValuePair in xmlProperties.CollectionProperties)
            {
                PropertyInfo property = propertyValuePair.Key;
                String elementName = xmlProperties.PropertyElementNames[property];
                String collectionInnerName = xmlProperties.CollectionInnerNames[property];
                XmlNodeList typeElements = node.SelectNodes("descendant::" + elementName + "/descendant::" + collectionInnerName);

                ConstructorInfo collectionConstructor = property.PropertyType.GetConstructor(new Type[0]);
                Object list = collectionConstructor.Invoke(null);
                Type collectionGenericType = property.PropertyType.GetGenericArguments()[0];
                MethodInfo addMethod = property.PropertyType.GetMethod("Add", new Type[] { collectionGenericType });

                for (Int32 i = 0; i < typeElements.Count; i++)
                {
                    XmlNode typeElement = typeElements.Item(i);
                    addMethod.Invoke(list, new Object[] { ReadNode(typeElement, collectionGenericType, propertyValuePair.Value) });
                }
                property.SetValue(obj, list);
            }

            foreach (KeyValuePair<PropertyInfo, XmlProperties> propertyValuePair in xmlProperties.ComplexProperties)
            {
                PropertyInfo property = propertyValuePair.Key;
                String elementName = xmlProperties.PropertyElementNames[property];
                XmlNode propertyNode = node.SelectSingleNode(elementName);
                if (propertyNode == null)
                    continue;
                Object value = ReadNode(propertyNode, property.PropertyType, propertyValuePair.Value);
                property.SetValue(obj, value);
            }
            return obj;
        }

        private static XmlProperties ReadTypes(Type type)
        {
            PropertyInfo[] properties = type.GetProperties();
            List<PropertyInfo> primitiveProperties = new List<PropertyInfo>();
            Dictionary<PropertyInfo, XmlProperties> collectionProperties = new Dictionary<PropertyInfo, XmlProperties>();
            Dictionary<PropertyInfo, XmlProperties> complexProperties = new Dictionary<PropertyInfo, XmlProperties>();
            Dictionary<PropertyInfo, String> propertyElementNames = new Dictionary<PropertyInfo, String>();
            Dictionary<PropertyInfo, String> collectionInnerNames = new Dictionary<PropertyInfo, String>();

            foreach (var property in properties.Where(p => p.CanWrite))
            {
                Type propertyType = property.PropertyType;

                XmlElementName attribute = (XmlElementName) property.GetCustomAttribute(typeof (XmlElementName));
                if (attribute == null)
                    continue;
                String elementName = attribute.Name ?? property.Name;
                propertyElementNames.Add(property, elementName);

                if (propertyType.IsPrimitive)
                    primitiveProperties.Add(property);
                else if (propertyType.GetInterfaces().Any(p => p.IsGenericType && p.GetGenericTypeDefinition() == typeof(ICollection<>)))
                {
                    XmlCollectionInnerName collectionAttribute = (XmlCollectionInnerName)property.GetCustomAttribute(typeof(XmlCollectionInnerName));
                    String innerName = collectionAttribute?.Name;
                    if (innerName == null)
                        throw new Exception("XmlCollectionInnerName must be set on ICollections!");
                    var arguments = propertyType.GetGenericArguments();
                    XmlProperties xmlProperties = ReadTypes(arguments[0]);
                    collectionProperties.Add(property, xmlProperties);
                    collectionInnerNames.Add(property, innerName);
                }
                else
                    complexProperties.Add(property, ReadTypes(propertyType));
            }

            return new XmlProperties { PrimitiveProperties = primitiveProperties, CollectionProperties = collectionProperties, ComplexProperties = complexProperties, PropertyElementNames = propertyElementNames, CollectionInnerNames = collectionInnerNames};
        }

        class XmlProperties
        {
            internal List<PropertyInfo> PrimitiveProperties { get; set; }
            internal Dictionary<PropertyInfo, XmlProperties> CollectionProperties { get; set; }
            internal Dictionary<PropertyInfo, XmlProperties> ComplexProperties { get; set; }

            internal Dictionary<PropertyInfo, String> PropertyElementNames { get; set; }
            internal Dictionary<PropertyInfo, String> CollectionInnerNames { get; set; } 
        }
    }
}