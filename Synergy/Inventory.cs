﻿using System;
using System.Collections.Generic;
using System.IO;
using Synergy.Installation;

namespace Synergy
{
    public class Inventory
    {
        public const Int32 InventoryWidth = 10;
        public const Int32 InventoryHeight = 8;

        public Item[,] Items { get; set; }

        public Dictionary<Int32, ItemType> ItemTypes { get; set; }

        public Inventory(Dictionary<Int32, ItemType> itemTypes)
        {
            Items = new Item[InventoryWidth, InventoryHeight];
            ItemTypes = itemTypes;
        }
        public void LoadInventory(Stream stream)
        {
            var reader = new BinaryReader(stream);
            Int32 version = reader.ReadInt32();
            switch (version)
            {
                case 1:
                    for (Int32 i = 0; i < InventoryWidth; i++)
                    {
                        for (Int32 j = 0; j < InventoryHeight; j++)
                        {
                            Boolean itemPresent = reader.ReadBoolean();
                            if (!itemPresent)
                                continue;
                            Int32 itemId = reader.ReadInt32();
                            Int32 itemVersion = reader.ReadInt32();
                            Int32 bytesWritten = reader.ReadInt32();
                            ItemCategory category = itemId > 0 ? ItemTypes[itemId].ItemCategory : ItemCategory.CubeStack;

                            Item item = Item.GetNewItem(itemId, category);
                            if (bytesWritten > 0)
                                item.Load(reader, itemVersion);
                            Items[i, j] = item;
                        }
                    }
                    break;
                default:
                    throw new Exception("File format (version " + version + ") of " + Player.InventoryFileName + " is not recognized!");
            }
        }

        public void WriteInventory(Stream stream, Int32 version)
        {
            BinaryWriter writer = new BinaryWriter(stream);
            writer.Write(version);
            switch (version)
            {
                case 1:
                    for (Int32 i = 0; i < InventoryWidth; i++)
                    {
                        for (Int32 j = 0; j < InventoryHeight; j++)
                        {
                            Item item = Items[i, j];
                            if (item == null)
                            {
                                writer.Write(false);
                                continue;
                            }
                            writer.Write(true);
                            writer.Write(item.ItemTypeId);
                            writer.Write(item.Version);
                            Int64 position = writer.BaseStream.Position;
                            writer.Write(0); // Placeholder
                            Int32 bytesWritten = item.Write(writer);
                            writer.BaseStream.Seek(position, SeekOrigin.Begin);
                            writer.Write(bytesWritten);
                            writer.BaseStream.Seek(bytesWritten, SeekOrigin.Current);
                        }
                    }
                    break;
                default:
                    throw new Exception("File format (version " + version + ") of " + Player.InventoryFileName + " is not recognized!");
            }
        }
    }
}
