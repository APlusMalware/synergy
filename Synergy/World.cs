﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Threading.Tasks;
using Synergy.Installation;
using Synergy.Terrain;

namespace Synergy
{
    public class World
    {
        [Obsolete("Use Path property instead")]
        public readonly String Location;
        public String FullPath { get; }
        public IDictionary<UInt16, CubeType> CubeList;

        public WorldSettings Settings { get; set; }
        public SegmentManager SegmentManager { get; set; }

        private World(String path)
        {
            Location = path;
            FullPath = path;
            SegmentManager = new SegmentManager(FullPath);
        }

        public static World Create(String path, WorldSettings settings)
        {
            World world = new World(path);
            Directory.CreateDirectory(path);
            if (File.Exists(Path.Combine(path, WorldSettings.SettingsFileName)))
                throw new Exception("World already exists!");
            world.Settings = settings;
            world.SaveSettings();
            return world;
        }

        public static World Read(String path)
        {
            World world = new World(path);
            String settingsPath = Path.Combine(path, WorldSettings.SettingsFileName);
            if (!File.Exists(settingsPath))
                return null;
            try
            {
                using (Stream s = File.Open(settingsPath, FileMode.Open))
                    world.Settings = new WorldSettings(s);
            }
            catch (FileNotFoundException)
            {
                return null;
            }
            return world;
        }

        public static List<World> ReadWorlds(String path)
        {
            return Directory.GetDirectories(path).Select(Read).Where(w => w != null).ToList();
        }

        public void SaveSettings()
        {
            using (Stream s = File.Open(Path.Combine(FullPath, WorldSettings.SettingsFileName), FileMode.Create))
                Settings.Write(s);
        }
        public void LoadDetailBlocks()
        {
            DetailBlock[] detailBlocks;
            using (var fs = File.Open(Path.Combine(FullPath, DetailBlock.DetailBlockSlotFileName), FileMode.Open))
            {
                var reader = new BinaryReader(fs);
                String[] slots = DetailBlock.ReadSlots(reader);
                DetailBlock[] worldDetailBlocks = DetailBlock.ReadDetailBlocks(Path.Combine(FullPath, DetailBlock.WorldDetailBlockDirectory));
                detailBlocks = new DetailBlock[slots.Length];
                for (Int32 i = 0; i < slots.Length; i++)
                {
                    detailBlocks[i] = worldDetailBlocks.FirstOrDefault(block => block.Name == slots[i]);
                }
            }
        }
        public List<String> Zip()
        {
            ConcurrentBag<String> failedDirectories = new ConcurrentBag<String>();
            List<String> completelyFailedDirectories = new List<String>();
            String[] directories = Directory.GetDirectories(Path.Combine(FullPath, "Segments"));
            Parallel.ForEach(directories, directory =>
            {
                String fileName = directory + ".zip";
                try
                {
                    ZipFile.CreateFromDirectory(directory, fileName);
                    Directory.Delete(directory, true);
                }
                catch (IOException)
                {
                    failedDirectories.Add(directory);
                }
            });
            // Retry failed folders once
            foreach(String directory in failedDirectories)
            {
                try
                {
                    ZipFile.CreateFromDirectory(directory, directory + ".zip");
                }
                catch (IOException)
                {
                    completelyFailedDirectories.Add(directory);
                }
            }
            return completelyFailedDirectories;
        }

        public override String ToString()
        {
            return $"{Settings.Name} ({new DirectoryInfo(FullPath).Name}) - {Settings.GameMode}";
        }
    }
}