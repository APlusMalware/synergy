﻿using System;
using System.IO;
using Synergy.Terrain;

namespace Synergy
{
    public enum GameMode
    {
        Creative,
        Survival
    }

    public enum DeathEffect
    {
        Easy,
        Ironman,
        Hardcore
    }

    public class WorldSettings
    {
        public Int32 SettingsVersion { get; set; }
        public Int32 WorldVersion { get; set; }
        public String Name { get; set; }
        public Int32 WorldSeed { get; set; }
        public Single Gravity { get; set; }
        public Single MovementSpeed { get; set; }
        public Single JumpSpeed { get; set; }
        public Single MaxFallingSpeed { get; set; }
        public GameMode GameMode { get; set; }
        public String InjectionSet { get; set; }
        public Int32 ResourceLevel { get; set; }
        public Int32 PowerLevel { get; set; }
        public Int32 ConveyorLevel { get; set; }
        public Int32 DayLevel { get; set; }
        public DeathEffect DeathEffect { get; set; }
        public CubeCoords SpawnLocation { get; set; }
        public Int32 MobLevel { get; set; }
        public Boolean IsIntroCompleted { get; set; }

        public static readonly String SettingsFileName = "world.dat";

        public WorldSettings()
        {
        }

        public WorldSettings(Stream stream)
        {
            try
            {
                BinaryReader reader = new BinaryReader(stream);
                SettingsVersion = reader.ReadInt32();
                WorldVersion = reader.ReadInt32();
                Name = reader.ReadString();
                WorldSeed = reader.ReadInt32();
                Gravity = reader.ReadSingle();
                MovementSpeed = reader.ReadSingle();
                JumpSpeed = reader.ReadSingle();
                MaxFallingSpeed = reader.ReadSingle();
                GameMode = (GameMode) reader.ReadInt32();
                InjectionSet = reader.ReadString();
                ResourceLevel = reader.ReadInt32();
                PowerLevel = reader.ReadInt32();
                ConveyorLevel = reader.ReadInt32();
                DayLevel = reader.ReadInt32();
                DeathEffect = (DeathEffect) reader.ReadInt32();
                SpawnLocation = new CubeCoords(reader.ReadInt64(), reader.ReadInt64(), reader.ReadInt64());
                IsIntroCompleted = reader.ReadBoolean();
                MobLevel = reader.ReadInt32();
            }
            catch (EndOfStreamException)
            {
                // Since the version numbers aren't incremented even when the format changes, 
                // we'll just catch the end of stream exception and pretend everything is ok.
                // That's how FortressCraft is doing it...
            }
        }

        public void Write(Stream stream)
        {
            BinaryWriter writer = new BinaryWriter(stream);
            writer.Write(SettingsVersion);
            switch (SettingsVersion)
            {
                case 1:
                    writer.Write(WorldVersion);
                    writer.Write(Name);
                    writer.Write(WorldSeed);
                    writer.Write(Gravity);
                    writer.Write(MovementSpeed);
                    writer.Write(JumpSpeed);
                    writer.Write(MaxFallingSpeed);
                    writer.Write((Int32)GameMode);
                    writer.Write(InjectionSet);
                    writer.Write(ResourceLevel);
                    writer.Write(PowerLevel);
                    writer.Write(ConveyorLevel);
                    writer.Write(DayLevel);
                    writer.Write((Int32)DeathEffect);
                    writer.Write(SpawnLocation.X);
                    writer.Write(SpawnLocation.Y);
                    writer.Write(SpawnLocation.Z);
                    writer.Write(IsIntroCompleted);
                    writer.Write(MobLevel);
                    break;
                default:
                    throw new Exception("File format (version " + SettingsVersion + ") is not recognized!");
            }
        }
    }
}