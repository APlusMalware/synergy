﻿using System;
using System.IO;
using System.Numerics;
using Synergy.Installation;
using Synergy.Terrain;

namespace Synergy
{
    public class Item
    {
        public ItemCategory ItemCategory { get; set; }
        public Int32 ItemTypeId { get; set; }

        public virtual Int32 Version
        {
            get { return 0; }
        }

        public virtual void Load(BinaryReader reader, Int32 version)
        {
        }

        public virtual Int32 Write(BinaryWriter writer)
        {
            return 0;
        }

        public static Item GetNewItem(ItemType type)
        {
            return GetNewItem(type.ItemId, type.ItemCategory);
        }

        public static Item GetNewItem(Int32 typeId, ItemCategory itemCategory)
        {
            Item item;
            switch (itemCategory)
            {
                case ItemCategory.Single:
                    item = new Item { ItemTypeId = typeId, ItemCategory = ItemCategory.Single };
                    break;
                case ItemCategory.Stack:
                    item = new ItemStack() { ItemTypeId = typeId, ItemCategory = ItemCategory.Stack };
                    break;
                case ItemCategory.CubeStack:
                    // All cube stacks have a negative TypeId
                    item = new ItemCubeStack() { ItemTypeId = -1, ItemCategory = ItemCategory.CubeStack };
                    break;
                case ItemCategory.Durability:
                    item = new ItemDurability { ItemTypeId = typeId, ItemCategory = ItemCategory.Durability };
                    break;
                case ItemCategory.Charge:
                    item = new ItemCharge { ItemTypeId = typeId, ItemCategory = ItemCategory.Charge };
                    break;
                case ItemCategory.Location:
                    item = new ItemLocation { ItemTypeId = typeId, ItemCategory = ItemCategory.Location };
                    break;
                default:
                    item = new Item { ItemTypeId = typeId, ItemCategory = ItemCategory.Unknown };
                    break;
            }
            return item;
        }
    }

    public class ItemStack : Item
    {
        public Int32 Amount { get; set; }

        public override void Load(BinaryReader reader, Int32 version)
        {
            switch (version)
            {
                case 0:
                    Amount = reader.ReadInt32();
                    break;
                default:
                    throw new Exception("Item version not recognized");
            }
        }
        public override Int32 Write(BinaryWriter writer)
        {
            writer.Write(Amount);
            return sizeof (Int32);
        }
    }

    public class ItemCubeStack : Item
    {
        public UInt16 CubeTypeId { get; set; }
        public UInt16 CubeData { get; set; }
        public Int32 Amount { get; set; }
        public override void Load(BinaryReader reader, Int32 version)
        {
            switch (version)
            {
                case 0:
                    CubeTypeId = reader.ReadUInt16();
                    CubeData = reader.ReadUInt16();
                    Amount = reader.ReadInt32();
                    break;
                default:
                    throw new Exception("Item version not recognized");
            }
        }
        public override Int32 Write(BinaryWriter writer)
        {
            writer.Write(CubeTypeId);
            writer.Write(CubeData);
            writer.Write(Amount);
            return sizeof(UInt16) * 2 + sizeof(Int32);
        }
    }

    public class ItemDurability : Item
    {
        public Single CurrentDurability { get; set; }
        public Single MaxDurablitity { get; set; }
        public override void Load(BinaryReader reader, Int32 version)
        {
            switch (version)
            {
                case 0:
                    CurrentDurability = reader.ReadSingle();
                    MaxDurablitity = reader.ReadSingle();
                    break;
                default:
                    throw new Exception("Item version not recognized");
            }
        }
        public override Int32 Write(BinaryWriter writer)
        {
            writer.Write(CurrentDurability);
            writer.Write(MaxDurablitity);
            return sizeof (Single)*2;
        }
    }
    public class ItemCharge : Item
    {
        public Single Charge { get; set; }
        public override void Load(BinaryReader reader, Int32 version)
        {
            switch (version)
            {
                case 0:
                    Charge = reader.ReadSingle();
                    break;
                default:
                    throw new Exception("Item version not recognized");
            }
        }
        public override Int32 Write(BinaryWriter writer)
        {
            writer.Write(Charge);
            return sizeof (Single);
        }
    }

    public class ItemLocation : Item
    {
        public CubeCoords Location { get; set; }
        public Vector3 Look { get; set; }
        public override void Load(BinaryReader reader, Int32 version)
        {
            switch (version)
            {
                case 0:
                    Location = new CubeCoords(reader.ReadInt64(), reader.ReadInt64(), reader.ReadInt64());
                    Look = new Vector3(reader.ReadSingle(), reader.ReadSingle(), reader.ReadSingle());
                    break;
                default:
                    throw new Exception("Item version not recognized");
            }
        }
        public override Int32 Write(BinaryWriter writer)
        {
            writer.Write(Location.X);
            writer.Write(Location.Y);
            writer.Write(Location.Z);

            writer.Write(Look.X);
            writer.Write(Look.Y);
            writer.Write(Look.Z);
            return sizeof (Single)*3 + sizeof (Int64)*3;
        }
    }
}
