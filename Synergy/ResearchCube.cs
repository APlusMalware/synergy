﻿using System;

namespace Synergy
{
    public struct ResearchCube
    {
        public UInt16 TypeId { get; }
        public Int32 Extra { get; }

        public ResearchCube(UInt16 typeId, Int32 extra)
        {
            TypeId = typeId;
            Extra = extra;
        }

        public static Boolean operator ==(ResearchCube left, ResearchCube right)
        {
            return left.TypeId == right.TypeId && left.Extra == right.Extra;
        }

        public static Boolean operator !=(ResearchCube left, ResearchCube right)
        {
            return !(left == right);
        }

        public override Boolean Equals(Object obj)
        {
            return obj is ResearchCube && this == (ResearchCube)obj;
        }

        public override Int32 GetHashCode()
        {
            return (TypeId << 16) | Extra;
        }
    }
}
