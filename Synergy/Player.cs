﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using Synergy.Installation;
using Synergy.Terrain;

namespace Synergy
{
    public class Player
    {
        public enum ResearchVersion
        {
            Zero = 0
        }
        public enum ScansVersion
        {
            Zero = 0
        }
        public enum SettingsVersion
        {
            Zero = 0
        }
        public enum InventoryVersion
        {
            One = 1
        }
        public static readonly String PlayersDirectory = "Players";
        public static readonly String ResearchFileName = "research.dat";
        public static readonly String ScanFileName = "scans.dat";
        public static readonly String SettingsFileName = "settings.dat";
        public static readonly String InventoryFileName = "SurvivalInventory_Items.dat";
        public World World { get; }
        public String Id { get; }

        public Int32 ResearchPoints { get; set; }
        public List<String> RecipeKeys { get; set; }
        public List<String> ProjectKeys { get; set; }
        public List<ResearchCube> KnownCubes { get; set; }
        public List<ResearchCube> ScannedCubes { get; set; }
        public PlayerSettings Settings { get; set; }
        public Inventory Inventory { get; set; }

        public Player(World world, String id)
        {
            World = world;
            Id = id;
        }

        public static List<Player> ReadPlayers(World world)
        {
            var players = new List<Player>();
            String path = Path.Combine(world.FullPath, Player.PlayersDirectory);
            Directory.CreateDirectory(path);
            foreach (String directory in Directory.GetDirectories(path))
            {
                String id = new DirectoryInfo(directory).Name;
                // Steam IDs are in hex, so only use directories that can be converted to hex
                UInt64 intId;
                if (UInt64.TryParse(id, NumberStyles.HexNumber, CultureInfo.InvariantCulture, out intId))
                {
                    Player player = new Player(world, id);
                    if (player.LoadSettingsFile())
                        players.Add(player);
                }
            }
            return players;
        }

        public Boolean LoadReasearchFile()
        {
            String filepath = Path.Combine(World.FullPath, PlayersDirectory, Id, ResearchFileName);
            MemoryStream ms;
            if (!File.Exists(filepath))
            {
                ResearchPoints = 0;
                RecipeKeys = new List<String>();
                ProjectKeys = new List<String>();
                return true;
            }
            try
            {
                using (var fs = File.OpenRead(filepath))
                {
                    ms = new MemoryStream(new Byte[fs.Length], 0, (Int32)fs.Length, true, true);
                    fs.Read(ms.GetBuffer(), 0, (Int32)fs.Length);
                }
            }
            catch (IOException)
            {
                return false;
            }
            var reader = new BinaryReader(ms);
            Int32 version = reader.ReadInt32();
            switch (version)
            {
                case 0:
                    Int32 researchPoints = reader.ReadInt32();
                    Int32 recipeCount = reader.ReadInt32();
                    String[] recipes = new String[recipeCount];
                    for (Int32 i = 0; i < recipeCount; i++)
                    {
                        recipes[i] = reader.ReadString();
                    }
                    Int32 projectCount = reader.ReadInt32();
                    String[] projects = new String[projectCount];
                    for (Int32 i = 0; i < projectCount; i++)
                    {
                        projects[i] = reader.ReadString();
                    }

                    ResearchPoints = researchPoints;
                    RecipeKeys = recipes.ToList();
                    ProjectKeys = projects.ToList();

                    break;
                default:
                    throw new Exception("File format (version " + version + ") of " + filepath + " is not recognized!");
            }
            return true;
        }

        public void SaveResearchFile(Int32 version)
        {
            String filepath = Path.Combine(World.FullPath, PlayersDirectory, Id, ResearchFileName);
            Int32 fileLength = 4*4 + RecipeKeys.Count + ProjectKeys.Count +
                               RecipeKeys.Aggregate(0, (a, b) => a + b.Length) +
                               ProjectKeys.Aggregate(0, (a, b) => a + b.Length);
            using (var ms = new MemoryStream(new Byte[fileLength], 0, fileLength, true, true))
            {
                var writer = new BinaryWriter(ms);
                writer.Write(version);
                switch (version)
                {
                    case 0:
                        writer.Write(ResearchPoints);
                        writer.Write(RecipeKeys.Count);
                        foreach (String recipeKey in RecipeKeys)
                            writer.Write(recipeKey);

                        writer.Write(ProjectKeys.Count());
                        foreach (String projectKey in ProjectKeys)
                            writer.Write(projectKey);
                        break;
                    default:
                        throw new Exception("File format (version " + version + ") of " + filepath +
                                            " is not recognized!");
                }
                File.WriteAllBytes(filepath, ms.GetBuffer());
            }
        }

        public Boolean LoadScansFile()
        {
            String filepath = Path.Combine(World.FullPath, PlayersDirectory, Id, ScanFileName);
            if (!File.Exists(filepath))
            {
                KnownCubes = new List<ResearchCube>();
                ScannedCubes = new List<ResearchCube>();
                return true;
            }
            MemoryStream ms;
            try
            {
                using (var fs = File.OpenRead(filepath))
                {
                    ms = new MemoryStream(new Byte[fs.Length], 0, (Int32)fs.Length, true, true);
                    fs.Read(ms.GetBuffer(), 0, (Int32)fs.Length);
                }
            }
            catch (IOException)
            {
                return false;
            }
            var reader = new BinaryReader(ms);
            Int32 version = reader.ReadInt32();
            switch (version)
            {
                case 0:
                    Int32 knownCubeCount = reader.ReadInt32();
                    ResearchCube[] knownCubes = new ResearchCube[knownCubeCount];
                    for (Int32 i = 0; i < knownCubeCount; i++)
                    {
                        UInt16 typeId = reader.ReadUInt16();
                        Int32 data = reader.ReadInt32();
                        knownCubes[i] = new ResearchCube(typeId, data);
                    }
                    Int32 scannedCubeCount = reader.ReadInt32();
                    ResearchCube[] scannedCubes = new ResearchCube[scannedCubeCount];
                    for (Int32 i = 0; i < scannedCubeCount; i++)
                    {
                        UInt16 typeId = reader.ReadUInt16();
                        Int32 data = reader.ReadInt32();
                        scannedCubes[i] = new ResearchCube(typeId, data);
                    }
                    
                    KnownCubes = knownCubes.ToList();
                    ScannedCubes = scannedCubes.ToList();

                    break;
                default:
                    throw new Exception("File format (version " + version + ") of " + filepath + " is not recognized!");
            }
            return true;
        }

        public void SaveScansFile(Int32 version)
        {
            String filepath = Path.Combine(World.FullPath, PlayersDirectory, Id, ScanFileName);
            Int32 fileLength = 3 * 4 + (sizeof(UInt16) + sizeof(Int32)) * (KnownCubes.Count + ScannedCubes.Count);
            using (var ms = new MemoryStream(new Byte[fileLength], 0, fileLength, true, true))
            {
                var writer = new BinaryWriter(ms);
                writer.Write(version);
                switch (version)
                {
                    case 0:
                        writer.Write(KnownCubes.Count);
                        foreach (ResearchCube cube in KnownCubes)
                        {
                            writer.Write(cube.TypeId);
                            writer.Write(cube.Extra);
                        }

                        writer.Write(ScannedCubes.Count);
                        foreach (ResearchCube cube in ScannedCubes)
                        {
                            writer.Write(cube.TypeId);
                            writer.Write(cube.Extra);
                        }
                        break;
                    default:
                        throw new Exception("File format (version " + version + ") of " + filepath +
                                            " is not recognized!");
                }
                File.WriteAllBytes(filepath, ms.GetBuffer());
            }
        }

        public Boolean LoadSettingsFile()
        {
            String filepath = Path.Combine(World.FullPath, PlayersDirectory, Id, SettingsFileName);
            if (!File.Exists(filepath))
            {
                Settings = new PlayerSettings();
                return true;
            }
            MemoryStream ms;
            try
            {
                using (var fs = File.OpenRead(filepath))
                {
                    ms = new MemoryStream(new Byte[fs.Length], 0, (Int32) fs.Length, true, true);
                    fs.Read(ms.GetBuffer(), 0, (Int32) fs.Length);
                }
            }
            catch (IOException)
            {
                return false;
            }
            Settings = new PlayerSettings();
            Settings.LoadSettings(ms);
            return true;
        }

        public void SaveSettingsFile(Int32 version)
        {
            String filepath = Path.Combine(World.FullPath, PlayersDirectory, Id, SettingsFileName);
            using (var ms = new MemoryStream())
            {
                Settings.WriteSettings(ms, version);
                Int32 length = (Int32)ms.Position;
                using (var fs = File.Open(filepath, FileMode.Create))
                {
                    fs.Write(ms.GetBuffer(), 0, length);
                }
            }
        }

        public Boolean LoadInventoryFile(Dictionary<Int32, ItemType> itemTypes)
        {
            String filepath = Path.Combine(World.FullPath, PlayersDirectory, Id, InventoryFileName);
            if (!File.Exists(filepath))
            {
                Inventory = new Inventory(itemTypes);
                return true;
            }
            MemoryStream ms;
            try
            {
                using (var fs = File.OpenRead(filepath))
                {
                    ms = new MemoryStream(new Byte[fs.Length], 0, (Int32) fs.Length, true, true);
                    fs.Read(ms.GetBuffer(), 0, (Int32) fs.Length);
                }
            }
            catch (IOException)
            {
                return false;
            }
            Inventory inventory = new Inventory(itemTypes);
            inventory.LoadInventory(ms);
            Inventory = inventory;
            return true;
        }

        public void SaveInventoryFile(Int32 version)
        {
            String filepath = Path.Combine(World.FullPath, PlayersDirectory, Id, InventoryFileName);
            using (var ms = new MemoryStream())
            {
                Inventory.WriteInventory(ms, version);
                Int32 length = (Int32)ms.Position;
                using (var fs = File.Open(filepath, FileMode.Create))
                {
                    fs.Write(ms.GetBuffer(), 0, length);
                }
            }
        }

        public override String ToString()
        {
            return Id;
        }
    }
}
