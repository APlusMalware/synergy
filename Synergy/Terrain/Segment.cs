﻿using System;
using System.IO;

namespace Synergy.Terrain
{
    public class Segment
	{
		private Cube[,,] _cubeData;

		public const int SegmentX = 16;
        public const int SegmentY = 16;
        public const int SegmentZ = 16;
        public const int SegmentX1 = 15;
        public const int SegmentY1 = 15;
        public const int SegmentZ1 = 15;
        public const int SegmentYShift = 8;
        public const int SegmentZShift = 4;
        public const int SegmentVolume = 4096;
        public long X;
        public long Y;
        public long Z;
        public long FileX;
        public long FileY;
        public long FileZ;
        public SegmentManager SegmentManager;
        public bool HasFaces;
        public bool IsEmpty;
        public bool IsBlank = false;
        public string FullFileName = "";

        private const Int32 DataBlockCount = 5;
        private const Int32 CubeBlockIdentifier = 0;
        private const Int32 EntityBlockIdentifier = 1;
        private const Int32 LightingBlockIdentifier = 2;
        private const Int32 BiomeBlockIdentifier = 3;
        private const Int32 ItemBlockIdentifier = 4;

        public SegmentCoords Coords
		{
			get
			{
				return new SegmentCoords(new CubeCoords(X, Y, Z));
			}
			set
			{
				CubeCoords cubeCoords = new CubeCoords(value);
				X = cubeCoords.X;
				Y = cubeCoords.Y;
				Z = cubeCoords.Z;
			}
		}
		public Cube[,,] CubeData
		{
			get
			{
				if (_cubeData == null)
					_cubeData = Segment.GetBlankSegment().CubeData;
				return _cubeData;
            }
			set
			{
				_cubeData = value;
			}
		}

		public Segment(SegmentManager segmentManager, SegmentCoords coords) : this(segmentManager, new CubeCoords(coords))
		{
		}
		public Segment(SegmentManager segmentManager, CubeCoords coords) : this(segmentManager, coords.X, coords.Y, coords.Z)
		{
		}
        public Segment(SegmentManager segmentManager, long baseX, long baseY, long baseZ)
        {
            X = baseX;
            Y = baseY;
            Z = baseZ;
            FileX = baseX;
            FileY = baseY;
            FileZ = baseZ;
            SegmentManager = segmentManager;
            _cubeData = new Cube[16, 16, 16];
        }

        public static Segment GetBlankSegment()
        {
            Segment blankSegment = new Segment(null, -1L, -1L, -1L);
            Cube[,,] array = new Cube[16, 16, 16];
            for (int i = 0; i < 16; i++)
            {
                for (int j = 0; j < 16; j++)
                {
                    for (int k = 0; k < 16; k++)
                    {
                        array[i, j, k] = new Cube(1, 0, 0, 0);
                    }
                }
            }
            blankSegment.CubeData = array;
            blankSegment.IsBlank = true;
            blankSegment.IsEmpty = true;
            return blankSegment;
        }
        public void LoadSegment(Stream stream)
        {
            BinaryReader binaryReader = new BinaryReader(stream);
            string a = binaryReader.ReadString();
            if (a != "FCU")
            {
                throw new Exception("Segment (" + X + " +  " + Y + " +  " + Z + ") file ID is not of FCU");
            }
            // Number of data blocks - currently Cube Data, Lighting Data, Entity Data, and Item Data
            Int32 dataBlockCount = binaryReader.ReadInt32();
            FileX = binaryReader.ReadInt64();
            FileY = binaryReader.ReadInt64();
            FileZ = binaryReader.ReadInt64();
            if (new SegmentCoords(X, Y, Z) != new SegmentCoords(FileX, FileY, FileZ))
            {
                throw new Exception("Segment (" + X + ", " + Y + ", " + Z + ") does not contain the correct coordinates");
            }
            long position = binaryReader.BaseStream.Position;
            binaryReader.BaseStream.Position = position;
            for (int i = 0; i < dataBlockCount; i++)
            {
                int dataBlockIdentifier = binaryReader.ReadInt32();
                int dataBlockVersion = binaryReader.ReadInt32();
                switch (dataBlockIdentifier)
                {
                    case CubeBlockIdentifier:
                        LoadCubeDataBlock(binaryReader, dataBlockVersion);
                        break;
                    case EntityBlockIdentifier:
                        if (dataBlockVersion != 1)
                        {
                            throw new Exception("Version mismatch!");
                        }
                        LoadEntityBlock(binaryReader, false, dataBlockVersion);
                        break;
                    case LightingBlockIdentifier:
                        LoadLightingBlock(binaryReader, dataBlockVersion);
                        break;
                    case BiomeBlockIdentifier:
                        LoadBiomeBlock(binaryReader, dataBlockVersion);
                        break;
                    case ItemBlockIdentifier:
                        LoadItemBlock(binaryReader, dataBlockVersion);
                        break;
                }
            }
            binaryReader.Close();
			if (IsEmpty)
				_cubeData = GetBlankSegment().CubeData;
        }

        private void LoadCubeDataBlock(BinaryReader reader, int version)
        {
            HasFaces = reader.ReadBoolean();
            int num = reader.ReadInt32();
            if (num == 0)
            {
                IsEmpty = true;
            }
            else
            {
                long startPosition = reader.BaseStream.Position;
                int cubeReadCount = 0;
                int xCoord = 0;
                int zCoord = 0;
                int yCoord = 0;
                reader.BaseStream.Position = startPosition;
                while (reader.BaseStream.Position < startPosition + (long)num)
                {
                    if (cubeReadCount == _cubeData.Length)
                    {
                        break;
                    }
                    ushort typeId = reader.ReadUInt16();
                    byte flags = reader.ReadByte();
                    int runLength = 1;
                    ushort extraData;
                    ushort lighting;
                    if (typeId == 65535)
                    {
                        runLength = (int)(flags + 2);
                        typeId = reader.ReadUInt16();
                        flags = reader.ReadByte();
                        extraData = reader.ReadUInt16();
                        lighting = reader.ReadUInt16();
                    }
                    else
                    {
                        extraData = reader.ReadUInt16();
                        lighting = reader.ReadUInt16();
                    }
                    if (typeId == 65535)
                    {
                        throw new Exception("Corrupted file - found cube type MAX");
                    }
                    for (int i = 0; i < runLength; i++)
                    {
                        _cubeData[xCoord, yCoord, zCoord] = new Cube(typeId, flags, extraData, lighting);
                        xCoord++;
                        if (xCoord == 16)
                        {
                            xCoord = 0;
                            zCoord++;
                            if (zCoord == 16)
                            {
                                zCoord = 0;
                                yCoord++;
                                if (yCoord >= 16)
                                {
                                }
                            }
                        }
                    }
                    cubeReadCount += runLength;
                }
            }
        }
        public static string GetSegmentSubDir(string segmentPath, SegmentCoords coords)
        {
            SubdirectoryCoords subdirCoords = new SubdirectoryCoords(coords);
            string text = "d-" + subdirCoords.X.ToString("X") + "-" + subdirCoords.Y.ToString("X") + "-" + subdirCoords.Z.ToString("X");
            string path = segmentPath + text;
            if (!Directory.Exists(path))
                Directory.CreateDirectory(path);
            return text;
        }
        public static string GetSegmentFileName(string segmentPath, SegmentCoords coords)
        {
            SubdirectoryCoords subCoords = new SubdirectoryCoords(coords);
            String subDir = "d-" + subCoords.X.ToString("X") + "-" + subCoords.Y.ToString("X") + "-" + subCoords.Z.ToString("X");

            String file = "s-" + coords.X.ToString("X") + "-" + coords.Y.ToString("X") + "-" + coords.Z.ToString("X") + ".dat";

            return Path.Combine(subDir, file);
        }
        public string GetSegmentFileName()
        {
            return Segment.GetSegmentFileName(SegmentManager.SegmentPath, Coords);
        }
        public void WriteSegment(Stream stream)
        {
            BinaryWriter binaryWriter = new BinaryWriter(stream);
            // File ID
            binaryWriter.Write("FCU");
            // Number of data blocks - currently Cube Data, Lighting Data, Entity Data, Biome Data, and Item Data
            binaryWriter.Write(DataBlockCount);
            binaryWriter.Write(FileX);
            binaryWriter.Write(FileY);
            binaryWriter.Write(FileZ);
            WriteCubeDataBlock(binaryWriter);
            WriteLightingBlock(binaryWriter);
            // Biome data isn't even used by FortressCraft yet
            WriteBiomeBlock(binaryWriter);
            WriteEntityBlock(binaryWriter);
            WriteItemBlock(binaryWriter);
            binaryWriter.Close();
        }
        private void WriteEntityBlock(BinaryWriter writer)
        {
            writer.Write(EntityBlockIdentifier);
            // Version number
            writer.Write(1);

            // Number of segment entities
            writer.Write(0);

            // TODO: Actually write segment entity instead of skipping over them
            // Normally segment entity data would be saved here, but since it saving data is, 
            // different for each entity, we don't do that currently.
        }
        private void WriteLightingBlock(BinaryWriter writer)
        {
            writer.Write(LightingBlockIdentifier);

            // Version number
            writer.Write(0);

            // TODO: write the lighting data
            // Light status
            writer.Write(0);
            // Light edge status
            writer.Write(0);
            // Sun lit
            writer.Write(true);
            // Redo sun light
            writer.Write(true);
            // Redo light
            writer.Write(true);
        }
        private void LoadLightingBlock(BinaryReader reader, int version)
        {
            reader.ReadInt32();
            reader.ReadInt32();
            reader.ReadBoolean();
            reader.ReadBoolean();
            reader.ReadBoolean();
        }
        private void LoadBiomeBlock(BinaryReader reader, int version)
        {
        }
        private void LoadEntityBlock(BinaryReader reader, bool fromNetwork, int version)
        {
            int num = reader.ReadInt32();
            for (int i = 0; i < num; i++)
            {
                int num2 = reader.ReadInt32();
                int num3 = reader.ReadInt32();
                if (fromNetwork)
                {
                    int num4 = reader.ReadInt32();
                }
                long num5 = reader.ReadInt64();
                long num6 = reader.ReadInt64();
                long num7 = reader.ReadInt64();
            }
            if (num > 0)
            {
            }
        }

        private void WriteCubeDataBlock(BinaryWriter writer)
        {
            writer.Write(CubeBlockIdentifier);
            // Version number
            writer.Write(0);
            writer.Write(HasFaces);
            if (IsEmpty)
            {
                // Empty segment
                writer.Write(0);
                return;
            }
            long beginningOffset = writer.BaseStream.Position;
            // Place holder for number of bytes written
            writer.Write(1);
            int totalCubeCount = _cubeData.Length;
            int byteWrittenCount = 0;
            int xCoord = 0;
            int zCoord = 0;
            int yCoord = 0;
            Cube cube = _cubeData[xCoord, yCoord, zCoord];
            int runLength = 1;
            int cubeWrittenCount = 1;
            while (cubeWrittenCount < totalCubeCount)
            {
                xCoord++;
                if (xCoord == 16)
                {
                    xCoord = 0;
                    zCoord++;
                    if (zCoord == 16)
                    {
                        zCoord = 0;
                        yCoord++;
                        if (yCoord >= 16)
                        {
                            throw new Exception("Writing too many cubes");
                        }
                    }
                }
                Cube cube2 = _cubeData[xCoord, yCoord, zCoord];
                // If the cube is the same as the last cube, use RLE compression
                // Run length is stored as a byte, but since we know that it will
                // be at least 2 when stored, we can encode 2 more if we subtract
                // 2 from run length before storage
                if (cube2 == cube && runLength < Byte.MaxValue + 2)
                {
                    runLength++;
                    cubeWrittenCount++;
                }
                else
                {
                    if (runLength > 1)
                    {
                        // If there is a run, write the maxiumum allowed type id
                        // to indicate it and then the length of the run
                        writer.Write(UInt16.MaxValue);
                        writer.Write((Byte)(runLength - 2));
                        byteWrittenCount += 3;
                    }
                    writer.Write(cube.TypeId);
                    writer.Write(cube.Flags);
                    writer.Write(cube.Extra);
                    writer.Write(cube.Lighting);
                    byteWrittenCount += 7;
                    if (cube.TypeId == 65535)
                    {
                        throw new Exception("Cannot save cube type MAX");
                    }
                    runLength = 1;
                    cube = cube2;
                    cubeWrittenCount++;
                }
            }
            if (runLength > 1)
            {
                writer.Write((UInt16)65535);
                writer.Write((Byte)(runLength - 2));
                byteWrittenCount += 3;
            }
            writer.Write(cube.TypeId);
            writer.Write(cube.Flags);
            writer.Write(cube.Extra);
            writer.Write(cube.Lighting);
            byteWrittenCount += 7;
            long endOffest = writer.BaseStream.Position;
            writer.BaseStream.Seek(beginningOffset, SeekOrigin.Begin);
            writer.Write(byteWrittenCount);
            writer.BaseStream.Seek(endOffest, SeekOrigin.Begin);
            if (cube.TypeId == 65535)
            {
                throw new Exception("cannot save cube type MAX");
            }
            if (cubeWrittenCount != totalCubeCount)
            {
                throw new Exception("compression failed. BUG!");
            }
            if (endOffest - (beginningOffset + 4) != byteWrittenCount)
            {
                throw new Exception("memwriting failed. BUG!");
            }
        }
        private void WriteBiomeBlock(BinaryWriter writer)
        {
            writer.Write(BiomeBlockIdentifier);
            // Version number
            writer.Write(0);
        }
        private void LoadItemBlock(BinaryReader reader, Int32 version)
        {

        }
        private void WriteItemBlock(BinaryWriter writer)
        {
            writer.Write(ItemBlockIdentifier);
            // Version number
            writer.Write(0);

            // Number of items
            writer.Write(0);

            // TODO: Actually write segment items instead of skipping over them
        }
    }
}
