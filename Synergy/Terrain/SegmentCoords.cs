﻿using System;
using System.Globalization;
namespace Synergy.Terrain
{
    public struct SegmentCoords
	{
		public Int64 X { get; }
		public Int64 Y { get; }
        public Int64 Z { get; }

		// This is the center of the world. Why it's this is a complete mystery.
		public static readonly SegmentCoords WorldCenter = new SegmentCoords(0x3FFFFFFFE000000, 0x3FFFFFFFE000000, 0x3FFFFFFFE000000);

        public static readonly SegmentCoords West = new SegmentCoords(-1, 0, 0);
        public static readonly SegmentCoords East = new SegmentCoords(1, 0, 0);
        public static readonly SegmentCoords Below = new SegmentCoords(0, -1, 0);
        public static readonly SegmentCoords Above = new SegmentCoords(0, 1, 0);
        public static readonly SegmentCoords South = new SegmentCoords(0, 0, -1);
        public static readonly SegmentCoords North = new SegmentCoords(0, 0, 1);

		public SegmentCoords(Int64 x, Int64 y, Int64 z)
        {
            X = x;
            Y = y;
            Z = z;
        }
		public SegmentCoords(CubeCoords coords)
		{
			X = coords.X >> 4;
			Y = coords.Y >> 4;
			Z = coords.Z >> 4;
		}
		public SegmentCoords(SubdirectoryCoords coords)
		{
			X = coords.X << 4;
			Y = coords.Y << 4;
			Z = coords.Z << 4;
		}
		public SegmentCoords(String fileName)
        {
            if (fileName.EndsWith(".dat"))
            {
                fileName = fileName.Substring(0, fileName.IndexOf("."));
            }
            String[] array = fileName.Split(new Char[]
            {
                '-'
            });
            X = Int64.Parse(array[1], NumberStyles.AllowHexSpecifier);
            Y = Int64.Parse(array[2], NumberStyles.AllowHexSpecifier);
            Z = Int64.Parse(array[3], NumberStyles.AllowHexSpecifier);
		}

		public static SegmentCoords operator +(SegmentCoords obj)
		{
			return new SegmentCoords(+obj.X, +obj.Y, +obj.Z);
		}
		public static SegmentCoords operator -(SegmentCoords obj)
		{
			return new SegmentCoords(-obj.X, -obj.Y, -obj.Z);
		}
		public static SegmentCoords operator +(SegmentCoords left, SegmentCoords right)
		{
			return new SegmentCoords(left.X + right.X, left.Y + right.Y, left.Z + right.Z);
		}
		public static SegmentCoords operator -(SegmentCoords left, SegmentCoords right)
		{
			return new SegmentCoords(left.X - right.X, left.Y - right.Y, left.Z - right.Z);
		}
		public static SegmentCoords operator *(SegmentCoords left, Int32 right)
		{
			return new SegmentCoords(left.X * right, left.Y * right, left.Z * right);
		}
		public static SegmentCoords operator /(SegmentCoords left, Int32 right)
		{
			return new SegmentCoords(left.X / right, left.Y / right, left.Z / right);
		}
		public static SegmentCoords operator %(SegmentCoords left, Int32 right)
		{
			return new SegmentCoords(left.X % right, left.Y % right, left.Z % right);
		}
		public static SegmentCoords operator *(Int32 left, SegmentCoords right)
		{
			return new SegmentCoords(right.X * left, right.Y * left, right.Z * left);
		}
		public static bool operator ==(SegmentCoords left, SegmentCoords right)
        {
            return left.X == right.X && left.Y == right.Y && left.Z == right.Z;
        }
        public static bool operator !=(SegmentCoords left, SegmentCoords right)
        {
            return !(left == right);
        }

        public override bool Equals(Object obj)
        {
            return this == (SegmentCoords)obj;
        }
        public override int GetHashCode()
        {
            return ((Int32)X << 21) | ((0x7FFFFC00 & (Int32)Y) << 11) | ((Int32)Z & 0x7FFFF800);
        }
        public override String ToString()
        {
            return $"X:{X}, Y:{Y}, Z:{Z}";
        }
    }
}
