﻿using System;
namespace Synergy.Terrain
{
    public struct Cube
	{
		public const Byte AboveFace = 0x01;
		public const Byte BelowFace = 0x02;
		public const Byte NorthFace = 0x08;
		public const Byte SouthFace = 0x04;
		public const Byte EastFace = 0x10;
		public const Byte WestFace = 0x20;

		public UInt16 TypeId { get; set; }
		public Byte Flags {	get; set; }
		public UInt16 Extra { get; set; }
		public UInt16 Lighting { get; set; }

        public bool IsAir
        {
            get { return TypeId == 1 || TypeId == 0; }
        }
        public Cube(UInt16 type, byte flags, ushort extraData, ushort lighting)
        {
            TypeId = type;
            Flags = flags;
            Extra = extraData;
            Lighting = lighting;
        }
        public static bool operator ==(Cube x, Cube y)
        {
            return x.Flags == y.Flags && x.Extra == y.Extra && x.TypeId == y.TypeId && x.Lighting == y.Lighting;
        }
        public static bool operator !=(Cube x, Cube y)
        {
            return !(x == y);
        }
        public override bool Equals(object obj)
        {
            return this == (Cube)obj;
        }
        public override int GetHashCode()
        {
            return (TypeId << 16) | Extra;
        }
    }
}
