﻿using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Synergy.Installation;

namespace Synergy.Terrain
{
	public class FlagPass
	{
		private World _world;
	    private SegmentManager _segmentManager;
		private ConcurrentBag<String> _failedSegments;
		public static IDictionary<UInt16, CubeType> CubeTypes { get; set; }

		private FlagPass(World world)
		{
			_world = world;
		    _segmentManager = world.SegmentManager;
			_failedSegments = new ConcurrentBag<String>();
		}

		public static List<String> FixWorld(World world)
		{
			ConcurrentDictionary<SubdirectoryCoords, SubDirectory> unprocessedFolders = new ConcurrentDictionary<SubdirectoryCoords, SubDirectory>();
			ConcurrentDictionary<SubdirectoryCoords, SubDirectory> nextPass = new ConcurrentDictionary<SubdirectoryCoords, SubDirectory>();
			ConcurrentDictionary<SubdirectoryCoords, SubDirectory> currentPass = new ConcurrentDictionary<SubdirectoryCoords, SubDirectory>();
			ConcurrentBag<String> failedSegments = new ConcurrentBag<String>();

			// Add folders to queue
			foreach (String folder in Directory.GetDirectories(world.SegmentManager.SegmentPath))
			{
				SubDirectory directory = new SubDirectory(folder);
				unprocessedFolders.TryAdd(directory.Coords, directory);
			}

			// Set each segment's neighbors
			foreach (KeyValuePair<SubdirectoryCoords, SubDirectory> folder in unprocessedFolders)
			{
				SubdirectoryCoords coords = folder.Key;
				SubDirectory directory = folder.Value;

				SubdirectoryCoords southDirectory = coords + SubdirectoryCoords.South;
				SubdirectoryCoords belowDirectory = coords + SubdirectoryCoords.Below;
				SubdirectoryCoords westDirectory = coords + SubdirectoryCoords.West;

				SubdirectoryCoords northDirectory = coords + SubdirectoryCoords.North;
				SubdirectoryCoords aboveDirectory = coords + SubdirectoryCoords.Above;
				SubdirectoryCoords eastDirectory = coords + SubdirectoryCoords.East;

				directory.SouthDirectory = unprocessedFolders.ContainsKey(southDirectory) ? unprocessedFolders[southDirectory] : null;
				directory.BelowDirectory = unprocessedFolders.ContainsKey(belowDirectory) ? unprocessedFolders[belowDirectory] : null;
				directory.WestDirectory = unprocessedFolders.ContainsKey(westDirectory) ? unprocessedFolders[westDirectory] : null;

				directory.NorthDirectory = unprocessedFolders.ContainsKey(northDirectory) ? unprocessedFolders[northDirectory] : null;
				directory.AboveDirectory = unprocessedFolders.ContainsKey(aboveDirectory) ? unprocessedFolders[aboveDirectory] : null;
				directory.EastDirectory = unprocessedFolders.ContainsKey(eastDirectory) ? unprocessedFolders[eastDirectory] : null;

				// Set up initial pass
				if (directory.SouthDirectory == null && directory.BelowDirectory == null && directory.WestDirectory == null)
				{
					nextPass.TryAdd(coords, directory);
				}
			}
			unprocessedFolders = null;

			// Run passes over directories until all segments have been passed
			while (nextPass.Count > 0)
			{
				currentPass = nextPass;
				nextPass = new ConcurrentDictionary<SubdirectoryCoords, SubDirectory>();

				// Prevent too many passes from being run at once and causing an out of memory error
				while (currentPass.Count > 16)
				{
					SubDirectory temp;
					currentPass.TryRemove(currentPass.Keys.First(), out temp);
					nextPass.TryAdd(temp.Coords, temp);
				}

				Parallel.ForEach(currentPass, (folder) =>
				{
					SubdirectoryCoords coords = folder.Key;
					SubDirectory directory = folder.Value;
					FlagPass pass = new FlagPass(world);

					pass.Run(directory);

					foreach (String file in pass._failedSegments)
					{
						failedSegments.Add(file);
					}

					if (directory.NorthDirectory != null)
					{
						directory.NorthDirectory.SouthDirectory = null;
						if (directory.NorthDirectory.BelowDirectory == null && directory.NorthDirectory.WestDirectory == null)
							nextPass.TryAdd(directory.NorthDirectory.Coords, directory.NorthDirectory);
					}
					if (directory.AboveDirectory != null)
					{
						directory.AboveDirectory.BelowDirectory = null;
						if (directory.AboveDirectory.SouthDirectory == null && directory.AboveDirectory.WestDirectory == null)
							nextPass.TryAdd(directory.AboveDirectory.Coords, directory.AboveDirectory);
					}
					if (directory.EastDirectory != null)
					{
						directory.EastDirectory.WestDirectory = null;
						if (directory.EastDirectory.SouthDirectory == null && directory.EastDirectory.BelowDirectory == null)
							nextPass.TryAdd(directory.EastDirectory.Coords, directory.EastDirectory);
					}
				});
			}

			return failedSegments.ToList();
		}

		private void Run(SubDirectory directory)
		{
			String path = _world.SegmentManager.SegmentPath + "d-" + directory.Coords.X.ToString("X") + "-" + directory.Coords.Y.ToString("X") + "-" + directory.Coords.Z.ToString("X");

			SegmentWrapper[,,] currentDirectorySegments = loadSegments(path);

			ConcurrentBag<SegmentWrapper> nextPass = new ConcurrentBag<SegmentWrapper>();
			ConcurrentBag<SegmentWrapper> currentPass;

			for (Byte i = 0; i < 16; i++)
			{
				for (Byte j = 0; j < 16; j++)
				{
					for (Byte k = 0; k < 16; k++)
					{
						SegmentWrapper current = currentDirectorySegments[i, j, k];
						if (current == null)
							continue;
						current.SouthSegment = (k > 0) ? currentDirectorySegments[i, j, k - 1] : null;
						current.BelowSegment = (j > 0) ? currentDirectorySegments[i, j - 1, k] : null;
						current.WestSegment = (i > 0) ? currentDirectorySegments[i - 1, j, k] : null;

						current.NorthSegment = (k < 16) ? currentDirectorySegments[i, j, k + 1] : null;
						current.AboveSegment = (j < 16) ? currentDirectorySegments[i, j + 1, k] : null;
						current.EastSegment = (i < 16) ? currentDirectorySegments[i + 1, j, k] : null;

						if (current.SouthSegment == null && current.BelowSegment == null && current.WestSegment == null)
						{
							nextPass.Add(current);
						}
					}
				}
			}

			while (nextPass.Count > 0)
			{
				currentPass = nextPass;
				nextPass = new ConcurrentBag<SegmentWrapper>();
				foreach (SegmentWrapper segment in currentPass)
				{
					passSegment(segment);

					// Add segments to next pass
					if (segment.NorthSegment != null)
					{
						segment.NorthSegment.SouthSegment = null;
						if (segment.NorthSegment.BelowSegment == null && segment.NorthSegment.WestSegment == null)
							nextPass.Add(segment.NorthSegment);
					}
					if (segment.AboveSegment != null)
					{
						segment.AboveSegment.BelowSegment = null;
						if (segment.AboveSegment.SouthSegment == null && segment.AboveSegment.WestSegment == null)
							nextPass.Add(segment.AboveSegment);
					}
					if (segment.EastSegment != null)
					{
						segment.EastSegment.WestSegment = null;
						if (segment.EastSegment.SouthSegment == null && segment.EastSegment.BelowSegment == null)
							nextPass.Add(segment.EastSegment);
					}

					while (true)
					{
						try
						{
							segment.CurrentSegment.WriteSegment(File.Open(segment.CurrentSegment.FullFileName, FileMode.OpenOrCreate));
							break;
						}
						catch (IOException)
						{
							continue;
						}
					}
				}
			}
		}

		private static void passSegment(SegmentWrapper segment)
		{
			Cube[,,] currentCubes = segment.CurrentSegment.CubeData;
			Cube[,,] northCubes;
			Cube[,,] aboveCubes;
			Cube[,,] eastCubes;

			if (segment.NorthSegment != null)
				northCubes = segment.NorthSegment.CurrentSegment.CubeData;
			else
				northCubes = null;
			if (segment.AboveSegment != null)
				aboveCubes = segment.AboveSegment.CurrentSegment.CubeData;
			else
				aboveCubes = null;
			if (segment.EastSegment != null)
				eastCubes = segment.EastSegment.CurrentSegment.CubeData;
			else
				eastCubes = null;

			Boolean empty = true;
			Boolean hasFaces = false;

			for (Byte i = 0; i < 16; i++)
			{
				for (Byte j = 0; j < 16; j++)
				{
					for (Byte k = 0; k < 16; k++)
					{
						CubeType currentType = CubeTypes[currentCubes[i, j, k].TypeId];
						CubeType northCubeType = k != 15 ? CubeTypes[currentCubes[i, j, k + 1].TypeId] : (northCubes != null ? CubeTypes[northCubes[i, j, 0].TypeId] : null);
						CubeType aboveCubeType = j != 15 ? CubeTypes[currentCubes[i, j + 1, k].TypeId] : (aboveCubes != null ? CubeTypes[aboveCubes[i, 0, k].TypeId] : null);
						CubeType eastCubeType = i != 15 ? CubeTypes[currentCubes[i + 1, j, k].TypeId] : (eastCubes != null ? CubeTypes[eastCubes[0, j, k].TypeId] : null);
						
						Byte flags = currentCubes[i, j, k].Flags;

						empty = empty && currentType.IsAir;

						if (k != 15)
						{
							if ((Boolean)northCubeType.IsHollow || ((Boolean)northCubeType.IsTransparent && northCubeType != currentType))
							{
								if (!currentType.IsAir && !currentType.HasObject)
								{
									flags |= Cube.NorthFace;
									hasFaces = true;
								}
							}
							if ((Boolean)currentType.IsHollow || ((Boolean)currentType.IsTransparent && northCubeType != currentType))
							{
								if (!northCubeType.IsAir && !northCubeType.HasObject)
									currentCubes[i, j, k + 1].Flags |= Cube.SouthFace;
								hasFaces = true;
							}
						}
						else if (northCubes != null)
						{
							if ((Boolean)northCubeType.IsHollow || ((Boolean)northCubeType.IsTransparent && northCubeType != currentType))
							{
								if (!currentType.IsAir && !currentType.HasObject)
								{
									flags |= Cube.NorthFace;
									hasFaces = true;
								}
							}
							if ((Boolean)currentType.IsHollow || ((Boolean)currentType.IsTransparent && northCubeType != currentType))
							{
								if (!northCubeType.IsAir && !northCubeType.HasObject)
									northCubes[i, j, 0].Flags |= Cube.SouthFace;
							}
						}

						if (j != 15)
						{
							if ((Boolean)aboveCubeType.IsHollow || ((Boolean)aboveCubeType.IsTransparent && aboveCubeType != currentType))
							{
								if (!currentType.IsAir && !currentType.HasObject)
								{
									flags |= Cube.AboveFace;
									hasFaces = true;
								}
							}
							if ((Boolean)currentType.IsHollow || ((Boolean)currentType.IsTransparent && aboveCubeType != currentType))
							{
								if (!aboveCubeType.IsAir && !aboveCubeType.HasObject)
									currentCubes[i, j + 1, k].Flags |= Cube.BelowFace;
								hasFaces = true;
							}
						}
						else if (aboveCubes != null)
                        {
							if ((Boolean)aboveCubeType.IsHollow || ((Boolean)aboveCubeType.IsTransparent && aboveCubeType != currentType))
							{
								if (!currentType.IsAir && !currentType.HasObject)
								{
									flags |= Cube.AboveFace;
									hasFaces = true;
								}
							}
							if ((Boolean)currentType.IsHollow || ((Boolean)currentType.IsTransparent && aboveCubeType != currentType))
							{
								if (!aboveCubeType.IsAir && !aboveCubeType.HasObject)
									aboveCubes[i, 0, k].Flags |= Cube.BelowFace;
							}
						}

						if (i != 15)
						{
							if ((Boolean)eastCubeType.IsHollow || ((Boolean)eastCubeType.IsTransparent && eastCubeType != currentType))
							{
								if (!currentType.IsAir && !currentType.HasObject)
								{
									flags |= Cube.EastFace;
									hasFaces = true;
								}
							}
							if ((Boolean)currentType.IsHollow || ((Boolean)currentType.IsTransparent && eastCubeType != currentType))
							{
								if (!eastCubeType.IsAir && !eastCubeType.HasObject)
									currentCubes[i + 1, j, k].Flags |= Cube.WestFace;
								hasFaces = true;
							}
						}
						else if (eastCubes != null)
						{
							if ((Boolean)eastCubeType.IsHollow || ((Boolean)eastCubeType.IsTransparent && eastCubeType != currentType))
							{
								if (!currentType.IsAir && !currentType.HasObject)
								{
									flags |= Cube.EastFace;
									hasFaces = true;
								}
							}
							if ((Boolean)currentType.IsHollow || ((Boolean)currentType.IsTransparent && eastCubeType != currentType))
							{
								if (!eastCubeType.IsAir && !eastCubeType.HasObject)
									eastCubes[0, j, k].Flags |= Cube.WestFace;
							}
						}
						currentCubes[i, j, k].Flags = flags;
                    }
				}
			}

			segment.CurrentSegment.IsEmpty = empty;
			segment.CurrentSegment.HasFaces = hasFaces && !empty;
		}

		private SegmentWrapper[,,] loadSegments(String path)
		{
			String[] files = Directory.GetFiles(path);

			SegmentWrapper[,,] currentDirectoryCache = new SegmentWrapper[17, 17, 17];

			ConcurrentBag<String> retry = new ConcurrentBag<String>();

			foreach (String fileName in files)
			{
				SegmentCoords coords = new SegmentCoords(Path.GetFileNameWithoutExtension(fileName));
				Int64 x = coords.X % 16;
				Int64 y = coords.Y % 16;
				Int64 z = coords.Z % 16;

				try
				{
					SegmentWrapper segmentWrapper = currentDirectoryCache[x, y, z] ?? new SegmentWrapper(_world.SegmentManager.GetSegment(coords));
					if (segmentWrapper.CurrentSegment == null)
						continue;
					if (z == 15)
					{
						Segment north = _world.SegmentManager.GetSegment(coords + SegmentCoords.North);
						currentDirectoryCache[x, y, 16] = north == null ? null : new SegmentWrapper(north);
					}
					if (y == 15)
					{
						Segment above = _world.SegmentManager.GetSegment(coords + SegmentCoords.Above);
						currentDirectoryCache[x, 16, z] = above == null ? null : new SegmentWrapper(above);
					}
					if (x == 15)
					{
						Segment east = _world.SegmentManager.GetSegment(coords + SegmentCoords.East);
						currentDirectoryCache[16, y, z] = east == null ? null : new SegmentWrapper(east);
					}
					currentDirectoryCache[x, y, z] = segmentWrapper;
				}
				catch (IOException)
				{
					// On IOException add segment to retry list for this directory
					retry.Add(fileName);
				}
			}

			// Retry each segment that failed
			foreach (String fileName in retry)
			{
				try
				{
					SegmentCoords coords = new SegmentCoords(Path.GetFileNameWithoutExtension(fileName));
					Int64 x = coords.X % 16;
					Int64 y = coords.Y % 16;
					Int64 z = coords.Z % 16;

					SegmentWrapper segmentWrapper = currentDirectoryCache[x, y, z] ?? new SegmentWrapper(_world.SegmentManager.GetSegment(coords));
					if (segmentWrapper.CurrentSegment == null)
						continue;
					if (z == 15)
					{
						Segment north = _world.SegmentManager.GetSegment(coords + SegmentCoords.North);
						currentDirectoryCache[x, y, 16] = north == null ? null : new SegmentWrapper(north);
					}
					if (y == 15)
					{
						Segment above = _world.SegmentManager.GetSegment(coords + SegmentCoords.Above);
						currentDirectoryCache[x, 16, z] = above == null ? null : new SegmentWrapper(above);
					}
					if (x == 15)
					{
						Segment east = _world.SegmentManager.GetSegment(coords + SegmentCoords.East);
						currentDirectoryCache[16, y, z] = east == null ? null : new SegmentWrapper(east);
					}
					currentDirectoryCache[x, y, z] = segmentWrapper;
				}
				catch (IOException)
				{
					_failedSegments.Add(fileName);
				}
			}
			return currentDirectoryCache;
		}
	}

	class SegmentWrapper
	{
		public SegmentWrapper SouthSegment { get; set; }
		public SegmentWrapper BelowSegment { get; set; }
		public SegmentWrapper WestSegment { get; set; }
		public SegmentWrapper NorthSegment { get; set; }
		public SegmentWrapper AboveSegment { get; set; }
		public SegmentWrapper EastSegment { get; set; }
		public Segment CurrentSegment { get; set; }

		public SegmentWrapper(Segment segment)
		{
			CurrentSegment = segment;

			SouthSegment = null;
			BelowSegment = null;
			WestSegment = null;
			NorthSegment = null;
			AboveSegment = null;
			EastSegment = null;
		}
	}
}
