﻿using System;
using System.Globalization;
namespace Synergy.Terrain
{
	public struct SubdirectoryCoords
	{
		public Int64 X { get; }
		public Int64 Y { get; }
		public Int64 Z { get; }

		// This is the center of the world. Why it's this is a complete mystery.
		public static readonly SubdirectoryCoords WorldCenter = new SubdirectoryCoords(0x3FFFFFFFE00000, 0x3FFFFFFFE00000, 0x3FFFFFFFE00000);

		public static readonly SubdirectoryCoords West = new SubdirectoryCoords(-1, 0, 0);
		public static readonly SubdirectoryCoords East = new SubdirectoryCoords(1, 0, 0);
		public static readonly SubdirectoryCoords Below = new SubdirectoryCoords(0, -1, 0);
		public static readonly SubdirectoryCoords Above = new SubdirectoryCoords(0, 1, 0);
		public static readonly SubdirectoryCoords South = new SubdirectoryCoords(0, 0, -1);
		public static readonly SubdirectoryCoords North = new SubdirectoryCoords(0, 0, 1);

		public SubdirectoryCoords(Int64 x, Int64 y, Int64 z) : this()
		{
			X = x;
			Y = y;
			Z = z;
		}
		public SubdirectoryCoords(SegmentCoords coords)
		{
			X = coords.X >> 4;
			Y = coords.Y >> 4;
			Z = coords.Z >> 4;
		}
		public SubdirectoryCoords(CubeCoords coords)
		{
			X = coords.X >> 8;
			Y = coords.Y >> 8;
			Z = coords.Z >> 8;
		}
		public SubdirectoryCoords(String fileName) : this()
		{
			String[] array = fileName.Split(new char[]
			{
				'-'
			});
			X = Int64.Parse(array[1], NumberStyles.AllowHexSpecifier);
			Y = Int64.Parse(array[2], NumberStyles.AllowHexSpecifier);
			Z = Int64.Parse(array[3], NumberStyles.AllowHexSpecifier);
		}

		public static SubdirectoryCoords operator +(SubdirectoryCoords obj)
		{
			return new SubdirectoryCoords(+obj.X, +obj.Y, +obj.Z);
		}
		public static SubdirectoryCoords operator -(SubdirectoryCoords obj)
		{
			return new SubdirectoryCoords(-obj.X, -obj.Y, -obj.Z);
		}
		public static SubdirectoryCoords operator +(SubdirectoryCoords left, SubdirectoryCoords right)
		{
			return new SubdirectoryCoords(left.X + right.X, left.Y + right.Y, left.Z + right.Z);
		}
		public static SubdirectoryCoords operator -(SubdirectoryCoords left, SubdirectoryCoords right)
		{
			return new SubdirectoryCoords(left.X - right.X, left.Y - right.Y, left.Z - right.Z);
		}
		public static SubdirectoryCoords operator *(SubdirectoryCoords left, Int32 right)
		{
			return new SubdirectoryCoords(left.X * right, left.Y * right, left.Z * right);
		}
		public static SubdirectoryCoords operator /(SubdirectoryCoords left, Int32 right)
		{
			return new SubdirectoryCoords(left.X / right, left.Y / right, left.Z / right);
		}
		public static SubdirectoryCoords operator %(SubdirectoryCoords left, Int32 right)
		{
			return new SubdirectoryCoords(left.X % right, left.Y % right, left.Z % right);
		}
		public static SubdirectoryCoords operator *(Int32 left, SubdirectoryCoords right)
		{
			return new SubdirectoryCoords(right.X * left, right.Y * left, right.Z * left);
		}
		public static Boolean operator ==(SubdirectoryCoords left, SubdirectoryCoords right)
		{
			return left.Equals(right);
		}
		public static Boolean operator !=(SubdirectoryCoords left, SubdirectoryCoords right)
		{
			return !left.Equals(right);
		}

		public override bool Equals(Object obj)
		{
			if ((Object)this == null || (Object)obj == null)
				return ((Object)this).Equals((Object)obj);
			SubdirectoryCoords coods = (SubdirectoryCoords)obj;
			return this.X == coods.X && this.Y == coods.Y && this.Z == coods.Z;
		}
		public override int GetHashCode()
		{
			return (Int32)((this.X << 22) | (this.Y << 11) | this.Z);
        }
        public override String ToString()
        {
            return $"X:{X}, Y:{Y}, Z:{Z}";
        }
    }
}
