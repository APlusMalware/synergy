﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Synergy.Terrain
{
    public class SegmentManager
    {
        public static readonly String SegmentsDirectory = "Segments";

        public String SegmentPath { get; set; }

        public SegmentManager(String worldPath)
        {
            SegmentPath = Path.Combine(worldPath, SegmentsDirectory);
        }

        public Stream GetStream(SegmentCoords coords)
        {
            String path = Path.Combine(SegmentPath, Segment.GetSegmentFileName(SegmentPath, coords));
            return GetStream(path);
        }
        public Stream GetStream(String path)
        {
            if (!File.Exists(path))
            {
                return null;
            }
            else
            {
                return File.Open(path, FileMode.OpenOrCreate);
            }
        }
        public Segment GetSegment(Int64 cubeX, Int64 cubeY, Int64 cubeZ)
        {
            return GetSegment(new CubeCoords(cubeX, cubeY, cubeZ));
        }
        public Segment GetSegment(CubeCoords coords)
        {
            return GetSegment(new SegmentCoords(coords));
        }
        public Segment GetSegment(SegmentCoords coords)
        {
            Segment result;
            Segment segment = new Segment(this, coords);
            segment.FullFileName = SegmentPath + Segment.GetSegmentFileName(SegmentPath, coords);
            //here
            Stream stream = GetStream(segment.FullFileName);
            if (stream == null)
            {
                result = null;
            }
            else
            {
                segment.LoadSegment(stream);
                if (segment.IsEmpty)
                    segment.CubeData = Segment.GetBlankSegment().CubeData;
                result = segment;
            }
            return result;
        }
        public Cube GetBlock(long x, long y, long z)
        {
            long num = x % 16L;
            long num2 = y % 16L;
            long num3 = z % 16L;
            long x2 = x - num;
            long y2 = y - num2;
            long z2 = z - num3;
            Segment segment = this.GetSegment(new CubeCoords(x2, y2, z2));
            if (segment != null)
                return segment.CubeData[num, num2, num3];
            else
                return new Cube(1, 0, 0, 0);
        }
    }
}
