﻿using System;

namespace Synergy.Terrain
{
	public struct CubeCoords
	{
		public Int64 X { get; }
		public Int64 Y { get; }
		public Int64 Z { get; }

		// This is the center of the world. Why it's this is a complete mystery.
		public static readonly CubeCoords WorldCenter = new CubeCoords(0x3FFFFFFFE0000000, 0x3FFFFFFFE0000000, 0x3FFFFFFFE0000000);

        public static readonly CubeCoords West = new CubeCoords(-1, 0, 0);
        public static readonly CubeCoords East = new CubeCoords(1, 0, 0);
        public static readonly CubeCoords Below = new CubeCoords(0, -1, 0);
        public static readonly CubeCoords Above = new CubeCoords(0, 1, 0);
        public static readonly CubeCoords South = new CubeCoords(0, 0, -1);
        public static readonly CubeCoords North = new CubeCoords(0, 0, 1);

		public CubeCoords(Int64 x, Int64 y, Int64 z)
		{
			X = x;
			Y = y;
			Z = z;
		}
		public CubeCoords(SegmentCoords coords)
		{
			X = coords.X << 4;
			Y = coords.Y << 4;
			Z = coords.Z << 4;
		}
		public CubeCoords(SubdirectoryCoords coords)
		{
			X = coords.X << 8;
			Y = coords.Y << 8;
			Z = coords.Z << 8;
		}

		public static CubeCoords operator +(CubeCoords obj)
		{
			return new CubeCoords(+obj.X, +obj.Y, +obj.Z);
		}
		public static CubeCoords operator -(CubeCoords obj)
		{
			return new CubeCoords(-obj.X, -obj.Y, -obj.Z);
		}
		public static CubeCoords operator +(CubeCoords left, CubeCoords right)
		{
			return new CubeCoords(left.X + right.X, left.Y + right.Y, left.Z + right.Z);
		}
		public static CubeCoords operator -(CubeCoords left, CubeCoords right)
		{
			return new CubeCoords(left.X - right.X, left.Y - right.Y, left.Z - right.Z);
		}
		public static CubeCoords operator *(CubeCoords left, Int32 right)
		{
			return new CubeCoords(left.X * right, left.Y * right, left.Z * right);
		}
		public static CubeCoords operator /(CubeCoords left, Int32 right)
		{
			return new CubeCoords(left.X / right, left.Y / right, left.Z / right);
		}
		public static CubeCoords operator %(CubeCoords left, Int32 right)
		{
			return new CubeCoords(left.X % right, left.Y % right, left.Z % right);
		}
		public static CubeCoords operator *(Int32 left, CubeCoords right)
		{
			return new CubeCoords(right.X * left, right.Y * left, right.Z * left);
		}
		public static bool operator ==(CubeCoords left, CubeCoords right)
		{
			return left.X == right.X && left.Y == right.Y && left.Z == right.Z;
		}
		public static bool operator !=(CubeCoords left, CubeCoords right)
		{
			return !(left == right);
		}

		public override bool Equals(Object obj)
		{
			return this == (CubeCoords)obj;
		}
		public override int GetHashCode()
		{
			return ((Int32)X << 21) | ((0x7FFFFC00 & (Int32)Y) << 11) | ((Int32)Z & 0x7FFFF800);
		}
	    public override String ToString()
	    {
	        return $"X:{X}, Y:{Y}, Z:{Z}";
	    }
	}
}
