﻿using System;

namespace Synergy.Terrain
{
	public class SubDirectory
	{
		public SubdirectoryCoords Coords { get; set; }

		public SubDirectory SouthDirectory { get; set; }
		public SubDirectory BelowDirectory { get; set; }
		public SubDirectory WestDirectory { get; set; }
		public SubDirectory NorthDirectory { get; set; }
		public SubDirectory AboveDirectory { get; set; }
		public SubDirectory EastDirectory { get; set; }

		public SubDirectory(Int64 x, Int64 y, Int64 z)
		{
			Coords = new SubdirectoryCoords(x, y, z);

			SouthDirectory = null;
			BelowDirectory = null;
			WestDirectory = null;
			NorthDirectory = null;
			AboveDirectory = null;
			EastDirectory = null;
		}

		public SubDirectory(SubdirectoryCoords coords)
		{
			Coords = coords;

			SouthDirectory = null;
			BelowDirectory = null;
			WestDirectory = null;
			NorthDirectory = null;
			AboveDirectory = null;
			EastDirectory = null;
		}

		public SubDirectory(String path)
		{
			Coords = new SubdirectoryCoords(path);

			SouthDirectory = null;
			BelowDirectory = null;
			WestDirectory = null;
			NorthDirectory = null;
			AboveDirectory = null;
			EastDirectory = null;
		}
	}
}
