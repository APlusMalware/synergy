﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Synergy
{
    public enum BlockBehavior
    {
        RotateCCW,
        RotateCW,
        Wind,
        Twizzle,
        Squish,
        Planetary,
        Piston,
        Orbit,
        Jiggle,
        GrowShrink,
        Dervish,
        Dangle,
        RandomiseFacing,
        Small,
        Big,
        Unified,
        Fountain,
        Smoke,
        Fire,
        GreenSmoke,
        Invisible,
        SlowFall,
        Passable,
        Ladder,
        Conveyor,
        JumpPad,
        FaceForwards,
        Door,
        Factory,
        Num
    }

    public class DetailBlockBehavior
    {
        public BlockBehavior BlockBehavior { get; set; }
        public Single Size { get; set; }
        public Single Speed { get; set; }
    }
}
