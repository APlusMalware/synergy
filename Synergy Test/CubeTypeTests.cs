﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using Synergy.Installation;
using Synergy.Xml;

namespace Synergy_Test
{
	[TestClass]
	public class CubeTypeTests
	{
		private IDictionary<UInt16, CubeType> _cubeList;

		public IDictionary<UInt16, CubeType> CubeList
		{
			get
			{
			    if (_cubeList == null)
			    {
                    XmlData xmlData = new XmlData(@"Test Files\Install\TerrainData.xml");
                    List<CubeType> cubeTypes = xmlData.ReadXmlToType<List<CubeType>>();
                    _cubeList = CubeType.CreateDictionary(cubeTypes);
			    }
			    return _cubeList;
			}
		}

		[TestMethod]
		public void CheckCubeListSize()
		{
			var asdf = CubeList.Count;
			Assert.IsTrue(CubeList.Count > 0);
		}

		[TestMethod]
		public void CheckTypeIdValid()
		{
			foreach (CubeType cubeType in CubeList.Values)
			{
				Assert.IsTrue(cubeType.TypeId > 0);
			}
		}
		[TestMethod]
		public void CheckSelectedCubeTypeValues()
		{
			List<ValueEntry> values = new List<ValueEntry>(6);
			values.Add(new ValueEntry { Value = 0, Name = "Diamond Crystal", IconName = "Diamond Crystal", Description = "", ResearchValue = 18 });
            values.Add(new ValueEntry { Value = 1, Name = "Emerald Crystal", IconName = "Emerald Crystal", Description = "", ResearchValue = 20 });
            values.Add(new ValueEntry { Value = 2, Name = "Ruby Crystal", IconName = "Ruby Crystal", Description = "", ResearchValue = 25 });
            values.Add(new ValueEntry { Value = 3, Name = "Sapphire Crystal", IconName = "Sapphire Crystal", Description = "", ResearchValue = 30 });
            values.Add(new ValueEntry { Value = 4, Name = "Topaz Crystal", IconName = "Topaz Crystal", Description = "", ResearchValue = 35 });
            values.Add(new ValueEntry { Value = 5, Name = "Sugalite Crystal", IconName = "Sugalite Crystal", Description = "", ResearchValue = 40 });

			List<Stage> stages = new List<Stage>(0);

			List<String> tags = new List<String>(2);
			tags.Add("Ore");
			tags.Add("Outdoor");

			CubeType known = new CubeType { TypeId = 162, Name = "Unknown Crystal", Description = null, ResearchValue = 5, Hardness = null, Values = values, DefaultValue = 0, LayerType = "PrimaryTerrain", TopTexture = 235, SideTexture = 235, BottomTexture = 235, GuiTexture = 235, Stages = stages, Hidden = null, IsSolid = false, IsTransparent = true, IsHollow = true, IsGlass = false, IsPassable = false, IsColorised = false, IsPaintable = false, HasObject = true, HasEntity = true, Category = null, AudioWalkType = "Dirt", AudioBuildType = "Dirt", AudioDestroyType = "None", Tags = tags};
			CubeType actual = CubeList[162];

			Assert.AreEqual(known.TypeId, actual.TypeId, "Type Ids do not match");
			Assert.AreEqual(known.Name, actual.Name, "Names do not match");
			Assert.AreEqual(known.Description, actual.Description, "Descriptions do not match");
			Assert.AreEqual(known.ResearchValue, actual.ResearchValue, "Research Values do not match");
			Assert.AreEqual(known.Hardness, actual.Hardness, "Hardnesses do not match");
			Assert.IsTrue(known.Values.SequenceEqual(actual.Values), "Values do not match");
			Assert.AreEqual(known.DefaultValue, actual.DefaultValue, "Default Values do not match");
			Assert.AreEqual(known.LayerType, actual.LayerType, "Layer Types do not match");
			Assert.AreEqual(known.SideTexture, actual.SideTexture, "Side Textures do not match");
			Assert.AreEqual(known.BottomTexture, actual.BottomTexture, "Bottom Textures do not match");
			Assert.AreEqual(known.GuiTexture, actual.GuiTexture, "Gui Textures do not match");
			Assert.IsTrue(known.Stages.SequenceEqual(actual.Stages), "Stages do not match");
			Assert.AreEqual(known.Hidden, actual.Hidden, "Hiddens do not match");
			Assert.AreEqual(known.IsSolid, actual.IsSolid, "IsS olids do not match");
			Assert.AreEqual(known.IsTransparent, actual.IsTransparent, "Is Transparents do not match");
			Assert.AreEqual(known.IsHollow, actual.IsHollow, "Is Hollows do not match");
			Assert.AreEqual(known.IsGlass, actual.IsGlass, "Is Glasss do not match");
			Assert.AreEqual(known.IsPassable, actual.IsPassable, "Is Passables do not match");
			Assert.AreEqual(known.IsColorised, actual.IsColorised, "Is Coloriseds do not match");
			Assert.AreEqual(known.IsPaintable, actual.IsPaintable, "Is Paintables do not match");
			Assert.AreEqual(known.HasObject, actual.HasObject, "Has Objects do not match");
			Assert.AreEqual(known.HasEntity, actual.HasEntity, "Has Entitys do not match");
			Assert.AreEqual(known.IsOpen, actual.IsOpen, "Is Opens do not match");
			Assert.AreEqual(known.Category, actual.Category, "Categorys do not match");
			Assert.AreEqual(known.AudioWalkType, actual.AudioWalkType, "Audio Walk Types do not match");
			Assert.AreEqual(known.AudioBuildType, actual.AudioBuildType, "Audio Build Types do not match");
			Assert.AreEqual(known.AudioDestroyType, actual.AudioDestroyType, "Audio Destroy Types do not match");
			Assert.IsTrue(known.Tags.SequenceEqual(actual.Tags), "Tags do not match");
		}
		[TestMethod]
		public void TestCubeTypeSaving()
        {
            Assert.Fail();
            /*
			String filePath = @"Test Files\Install\TerrainData - Saved.xml";
			CubeType.SaveToFile(filePath, new List<CubeType>(CubeList.Values));
			IDictionary<UInt16, CubeType> reloaded = CubeType.LoadFromFile(filePath);
			foreach (CubeType item in CubeList.Values)
			{
				CubeType known = item;
				CubeType actual = reloaded[item.TypeId];

				Assert.AreEqual(known.TypeId, actual.TypeId, "Type Ids do not match");
				Assert.AreEqual(known.Name, actual.Name, "Names do not match");
				Assert.AreEqual(known.Description, actual.Description, "Descriptions do not match");
				Assert.AreEqual(known.ResearchValue, actual.ResearchValue, "Research Values do not match");
				Assert.AreEqual(known.Hardness, actual.Hardness, "Hardnesses do not match");
				Assert.IsTrue(known.Values.SequenceEqual(actual.Values), "Values do not match");
				Assert.AreEqual(known.DefaultValue, actual.DefaultValue, "Default Values do not match");
				Assert.AreEqual(known.LayerType, actual.LayerType, "Layer Types do not match");
				Assert.AreEqual(known.SideTexture, actual.SideTexture, "Side Textures do not match");
				Assert.AreEqual(known.BottomTexture, actual.BottomTexture, "Bottom Textures do not match");
				Assert.AreEqual(known.GuiTexture, actual.GuiTexture, "Gui Textures do not match");
				Assert.IsTrue(known.Stages.SequenceEqual(actual.Stages), "Stages do not match");
				Assert.AreEqual(known.Hidden, actual.Hidden, "Hiddens do not match");
				Assert.AreEqual(known.IsSolid, actual.IsSolid, "Is Solids do not match");
				Assert.AreEqual(known.IsTransparent, actual.IsTransparent, "Is Transparents do not match {0} {1}", new String[] { known.TypeId.ToString(), actual.TypeId.ToString()});
				Assert.AreEqual(known.IsHollow, actual.IsHollow, "Is Hollows do not match");
				Assert.AreEqual(known.IsGlass, actual.IsGlass, "Is Glasses do not match");
				Assert.AreEqual(known.IsPassable, actual.IsPassable, "Is Passables do not match");
				Assert.AreEqual(known.IsColorised, actual.IsColorised, "Is Coloriseds do not match");
				Assert.AreEqual(known.IsPaintable, actual.IsPaintable, "Is Paintables do not match");
				Assert.AreEqual(known.HasObject, actual.HasObject, "Has Objects do not match");
				Assert.AreEqual(known.HasEntity, actual.HasEntity, "Has Entitys do not match");
				Assert.AreEqual(known.IsOpen, actual.IsOpen, "Is Opens do not match");
				Assert.AreEqual(known.Category, actual.Category, "Categorys do not match");
				Assert.AreEqual(known.AudioWalkType, actual.AudioWalkType, "Audio Walk Types do not match");
				Assert.AreEqual(known.AudioBuildType, actual.AudioBuildType, "Audio Build Types do not match");
				Assert.AreEqual(known.AudioDestroyType, actual.AudioDestroyType, "Audio Destroy Types do not match");
				Assert.IsTrue(known.Tags.SequenceEqual(actual.Tags), "Tags do not match");
			}

			foreach (CubeType item in reloaded.Values)
			{
				CubeType known = item;
				CubeType actual = CubeList[item.TypeId];

				Assert.AreEqual(known.TypeId, actual.TypeId, "Type Ids do not match");
				Assert.AreEqual(known.Name, actual.Name, "Names do not match");
				Assert.AreEqual(known.Description, actual.Description, "Descriptions do not match");
				Assert.AreEqual(known.ResearchValue, actual.ResearchValue, "Research Values do not match");
				Assert.AreEqual(known.Hardness, actual.Hardness, "Hardnesses do not match");
				Assert.IsTrue(known.Values.SequenceEqual(actual.Values), "Values do not match");
				Assert.AreEqual(known.DefaultValue, actual.DefaultValue, "Default Values do not match");
				Assert.AreEqual(known.LayerType, actual.LayerType, "Layer Types do not match");
				Assert.AreEqual(known.SideTexture, actual.SideTexture, "Side Textures do not match");
				Assert.AreEqual(known.BottomTexture, actual.BottomTexture, "Bottom Textures do not match");
				Assert.AreEqual(known.GuiTexture, actual.GuiTexture, "Gui Textures do not match");
				Assert.IsTrue(known.Stages.SequenceEqual(actual.Stages), "Stages do not match");
				Assert.AreEqual(known.Hidden, actual.Hidden, "Hiddens do not match");
				Assert.AreEqual(known.IsSolid, actual.IsSolid, "Is Solids do not match");
				Assert.AreEqual(known.IsTransparent, actual.IsTransparent, "Is Transparents do not match {0} {1}", new String[] { known.TypeId.ToString(), actual.TypeId.ToString() });
				Assert.AreEqual(known.IsHollow, actual.IsHollow, "Is Hollows do not match");
				Assert.AreEqual(known.IsGlass, actual.IsGlass, "Is Glasses do not match");
				Assert.AreEqual(known.IsPassable, actual.IsPassable, "Is Passables do not match");
				Assert.AreEqual(known.IsColorised, actual.IsColorised, "Is Coloriseds do not match");
				Assert.AreEqual(known.IsPaintable, actual.IsPaintable, "Is Paintables do not match");
				Assert.AreEqual(known.HasObject, actual.HasObject, "Has Objects do not match");
				Assert.AreEqual(known.HasEntity, actual.HasEntity, "Has Entitys do not match");
				Assert.AreEqual(known.IsOpen, actual.IsOpen, "Is Opens do not match");
				Assert.AreEqual(known.Category, actual.Category, "Categorys do not match");
				Assert.AreEqual(known.AudioWalkType, actual.AudioWalkType, "Audio Walk Types do not match");
				Assert.AreEqual(known.AudioBuildType, actual.AudioBuildType, "Audio Build Types do not match");
				Assert.AreEqual(known.AudioDestroyType, actual.AudioDestroyType, "Audio Destroy Types do not match");
				Assert.IsTrue(known.Tags.SequenceEqual(actual.Tags), "Tags do not match");
			}
            */
		}
	}
}
