﻿using System;
using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Synergy;

namespace Synergy_Test
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestWorldSettingsRead()
        {
            using (var fs = File.OpenRead(@"Test Files\World\world.dat"))
            {
                WorldSettings settings = new WorldSettings(fs);
            }
        }

        [TestMethod]
        public void TestWorldRead()
        {
            World world = World.Read(@"Test Files\World\");
        }
    }
}
