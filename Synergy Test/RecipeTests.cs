﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using Synergy.Installation;
using Synergy.Xml;

namespace Synergy_Test
{
	[TestClass]
	public class RecipeTests
	{
		private IDictionary<String, List<Recipe>> _recipeList;

		public IDictionary<String, List<Recipe>> RecipeList
		{
			get
			{
			    if (_recipeList == null)
			    {
			        XmlData xmlData = new XmlData(@"Test Files\Install\ManufacturerRecipes.xml");
			        List<Recipe> recipes = xmlData.ReadXmlToType<List<Recipe>>();
			        _recipeList = Recipe.CreateDictionary(recipes);
			    }
			    return _recipeList;
			}
		}

		[TestMethod]
		public void CheckRecipeListSize()
		{
			Assert.IsTrue(RecipeList.Count > 0);
		}

		[TestMethod]
		public void CheckRecipeKeysNotBlank()
		{
			foreach (Recipe recipe in RecipeList.Values.First())
			{
				Assert.IsTrue(recipe.Key != "");
			}
		}

		[TestMethod]
		public void CheckSelectedRecipeValues()
		{
			List<CraftCost> costs = new List<CraftCost>(3);
			costs.Add(new CraftCost{ Name = "Gold Bar", Amount = 15});
            costs.Add(new CraftCost { Name = "Nickel Bar", Amount = 32 });
            costs.Add(new CraftCost { Name = "Titanium Bar", Amount = 64 });

			Recipe known = new Recipe { Key = "PSB mk3", Category = "power", Tier = 2, CraftedName = "Power Storage MK3", CraftedAmount = 1, Costs = costs, ResearchCost = null, Description = "Stores up to 5000 power", Hint = "Connect to machines to power them", ResearchRequirements = new List<String>(), ScanRequirements = new List<ScanRequirement>(), CanCraftAnywhere = null};
			Recipe actual = RecipeList["PSB mk3"].First();

			Assert.AreEqual(known.Key, actual.Key, "Keys do not match");
			Assert.AreEqual(known.Category, actual.Category, "Categorys do not match");
			Assert.AreEqual(known.Tier, actual.Tier, "Tiers do not match");
			Assert.AreEqual(known.CraftedName, actual.CraftedName, "Crafted Names do not match");
			Assert.AreEqual(known.CraftedAmount, actual.CraftedAmount, "Crafted Amounts do not match");
			Assert.IsTrue(known.Costs.SequenceEqual(actual.Costs), "Costs do not match");
			Assert.AreEqual(known.ResearchCost, actual.ResearchCost, "Research Costs do not match");
			Assert.AreEqual(known.Description, actual.Description, "Descriptions do not match");
			Assert.AreEqual(known.Hint, actual.Hint, "Hints do not match");
			Assert.IsTrue(known.ResearchRequirements.SequenceEqual(actual.ResearchRequirements), "Research Requirements do not match");
			Assert.IsTrue(known.ScanRequirements.SequenceEqual(actual.ScanRequirements), "Scan Requirements do not match");
			Assert.AreEqual(known.CanCraftAnywhere, actual.CanCraftAnywhere, "Can Craft Anywheres do not match");
		}

		[TestMethod]
		public void TestRecipeSaving()
		{
            Assert.Fail();
            /*
			String filePath = @"Test Files\Install\ManufacturerRecipes - Saved.xml";
			Recipe.SaveToFile(filePath, new List<Recipe>(RecipeList.Values));
			IDictionary<String, Recipe> reloaded = Recipe.LoadFromFile(filePath);
			foreach (Recipe recipe in RecipeList.Values)
			{
				Recipe known = recipe;
				Recipe actual = reloaded[known.Key];

				Assert.AreEqual(known.Key, actual.Key, "Keys do not match");
				Assert.AreEqual(known.Category, actual.Category, "Categorys do not match");
				Assert.AreEqual(known.Tier, actual.Tier, "Tiers do not match");
				Assert.AreEqual(known.CraftedName, actual.CraftedName, "Crafted Names do not match");
				Assert.AreEqual(known.CraftedAmount, actual.CraftedAmount, "Crafted Amounts do not match");
				Assert.IsTrue(known.Costs.SequenceEqual(actual.Costs), "Costs do not match");
				Assert.AreEqual(known.ResearchCost, actual.ResearchCost, "Research Costs do not match");
				Assert.AreEqual(known.Description, actual.Description, "Descriptions do not match");
				Assert.AreEqual(known.Hint, actual.Hint, "Hints do not match");
				Assert.IsTrue(known.ResearchRequirements.SequenceEqual(actual.ResearchRequirements), "Research Requirements do not match");
				Assert.IsTrue(known.ScanRequirements.SequenceEqual(actual.ScanRequirements), "Scan Requirements do not match");
				Assert.AreEqual(known.CanCraftAnywhere, actual.CanCraftAnywhere, "Can Craft Anywheres do not match");
			}

			foreach (Recipe recipe in reloaded.Values)
			{
				Recipe known = recipe;
				Recipe actual = RecipeList[known.Key];

				Assert.AreEqual(known.Key, actual.Key, "Keys do not match");
				Assert.AreEqual(known.Category, actual.Category, "Categorys do not match");
				Assert.AreEqual(known.Tier, actual.Tier, "Tiers do not match");
				Assert.AreEqual(known.CraftedName, actual.CraftedName, "Crafted Names do not match");
				Assert.AreEqual(known.CraftedAmount, actual.CraftedAmount, "Crafted Amounts do not match");
				Assert.IsTrue(known.Costs.SequenceEqual(actual.Costs), "Costs do not match");
				Assert.AreEqual(known.ResearchCost, actual.ResearchCost, "Research Costs do not match");
				Assert.AreEqual(known.Description, actual.Description, "Descriptions do not match");
				Assert.AreEqual(known.Hint, actual.Hint, "Hints do not match");
				Assert.IsTrue(known.ResearchRequirements.SequenceEqual(actual.ResearchRequirements), "Research Requirements do not match");
				Assert.IsTrue(known.ScanRequirements.SequenceEqual(actual.ScanRequirements), "Scan Requirements do not match");
				Assert.AreEqual(known.CanCraftAnywhere, actual.CanCraftAnywhere, "Can Craft Anywheres do not match");
			}
            */
        }
    }
}
