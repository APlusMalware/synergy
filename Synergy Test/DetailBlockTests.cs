﻿using System;
using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using  Synergy;

namespace Synergy_Test
{
    [TestClass]
    public class DetailBlockTests
    {
        [TestMethod]
        public void TestBlockReading()
        {
            String filePath = @"Test Files\Detail Blocks\Unnamed.detailBlock";
            MemoryStream ms;
            using (var fs = File.Open(filePath, FileMode.Open))
            {
                Int64 length = fs.Length;
                ms = new MemoryStream(new Byte[length], 0, (Int32)length, true, true);
                fs.Read(ms.GetBuffer(), 0, (Int32)length);
            }
            var reader = new BinaryReader(ms);
            var block = new DetailBlock();
            block.Load(reader);
            Assert.AreEqual(block.Name, "Unnamed");
        }
    }
}
