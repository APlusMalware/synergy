﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using Synergy.Installation;
using Synergy.Xml;

namespace Synergy_Test
{
    [TestClass]
    public class ItemTests
    {
		private IDictionary<Int32, ItemType> _itemList;

		public IDictionary<Int32, ItemType> ItemList
		{
			get
            {
                if (_itemList == null)
                {
                    XmlData xmlData = new XmlData(@"Test Files\Install\Items.xml");
                    List<ItemType> itemTypes = xmlData.ReadXmlToType<List<ItemType>>();
                    _itemList = ItemType.CreateDictionary(itemTypes);
                }
                return _itemList;
			}
		}

		[TestMethod]
        public void CheckItemListSize()
        {
			Assert.IsTrue(ItemList.Count > 0);
        }

		[TestMethod]
		public void CheckItemIdValid()
		{
			foreach (ItemType item in ItemList.Values)
			{
				Assert.IsTrue(item.ItemId > 0);
			}
		}
		[TestMethod]
		public void CheckSelectedItemValues()
		{
			ItemType known = new ItemType { ItemId = 301, Name = "Chargeable Explosive", Plural = "Chargeable Explosives", Type = "ItemCharge", Hidden = null, ObjectDropped = "UnknownItem", Sprite = "Chargeable Explosive", Category = "Consumable", ItemAction = "BuildBlock", ActionParameter = "Deployed Explosive"};
			ItemType actual = ItemList[301];

            Assert.AreEqual(known.ItemId, actual.ItemId, "Item Ids do not match");
			Assert.AreEqual(known.Name, actual.Name, "Names do not match");
			Assert.AreEqual(known.Plural, actual.Plural, "Plurals do not match");
			Assert.AreEqual(known.Type, actual.Type, "Types do not match");
			Assert.AreEqual(known.Hidden, actual.Hidden, "Hiddens do not match");
			Assert.AreEqual(known.ObjectDropped, actual.ObjectDropped, "Objects Dropped do not match");
			Assert.AreEqual(known.Sprite, actual.Sprite, "Sprites do not match");
			Assert.AreEqual(known.Category, actual.Category, "Categorys do not match");
			Assert.AreEqual(known.ItemAction, actual.ItemAction, "Item Actions do not match");
			Assert.AreEqual(known.ActionParameter, actual.ActionParameter, "Action Parameters do not match");
		}

		[TestMethod]
		public void TestItemSaving()
        {
            Assert.Fail();
            /*
			String filePath = @"Test Files\Install\Items - Saved.xml";
			ItemType.SaveToFile(filePath, new List<ItemType>(ItemList.Values));
            IDictionary <UInt16, ItemType> reloaded = ItemType.LoadFromFile(filePath);
			foreach (ItemType item in ItemList.Values)
			{
				ItemType known = item;
				ItemType actual = reloaded[item.ItemId.Value];

				Assert.AreEqual(known.ItemId, actual.ItemId, "Item Ids do not match");
				Assert.AreEqual(known.Name, actual.Name, "Names do not match");
				Assert.AreEqual(known.Plural, actual.Plural, "Plurals do not match");
				Assert.AreEqual(known.Type, actual.Type, "Types do not match");
				Assert.AreEqual(known.Hidden, actual.Hidden, "Hiddens do not match");
				Assert.AreEqual(known.ObjectDropped, actual.ObjectDropped, "Objects Dropped do not match");
				Assert.AreEqual(known.Sprite, actual.Sprite, "Sprites do not match");
				Assert.AreEqual(known.Category, actual.Category, "Categorys do not match");
				Assert.AreEqual(known.ItemAction, actual.ItemAction, "Item Actions do not match");
				Assert.AreEqual(known.ActionParameter, actual.ActionParameter, "Action Parameters do not match");
			}

			foreach (ItemType item in reloaded.Values)
			{
				ItemType known = item;
				ItemType actual = ItemList[item.ItemId.Value];

				Assert.AreEqual(known.ItemId, actual.ItemId, "Item Ids do not match");
				Assert.AreEqual(known.Name, actual.Name, "Names do not match");
				Assert.AreEqual(known.Plural, actual.Plural, "Plurals do not match");
				Assert.AreEqual(known.Type, actual.Type, "Types do not match");
				Assert.AreEqual(known.Hidden, actual.Hidden, "Hiddens do not match");
				Assert.AreEqual(known.ObjectDropped, actual.ObjectDropped, "Objects Dropped do not match");
				Assert.AreEqual(known.Sprite, actual.Sprite, "Sprites do not match");
				Assert.AreEqual(known.Category, actual.Category, "Categorys do not match");
				Assert.AreEqual(known.ItemAction, actual.ItemAction, "Item Actions do not match");
				Assert.AreEqual(known.ActionParameter, actual.ActionParameter, "Action Parameters do not match");
			}
            */
		}
	}
}
