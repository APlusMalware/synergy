﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using Synergy.Installation;
using Synergy.Xml;

namespace Synergy_Test
{
    [TestClass]
    public class ResearchTests
    {
		private IDictionary<String, Research> _researchList;

		public IDictionary<String, Research> ResearchList
		{
			get
			{
			    if (_researchList == null)
			    {
                    XmlData xmlData = new XmlData(@"Test Files\Install\Research.xml");
                    List<Research> researches = xmlData.ReadXmlToType<List<Research>>();
                    _researchList = Research.CreateDictionary(researches);
			    }
			    return _researchList;
			}
		}

		[TestMethod]
        public void CheckResearchListSize()
        {
			Assert.IsTrue(ResearchList.Count > 0);
        }

		[TestMethod]
		public void CheckResearchKeysNotBlank()
		{
			foreach (Research research in ResearchList.Values)
			{
				Assert.IsTrue(research.Key != "");
			}
		}
		
		[TestMethod]
		public void CheckSelectedResearchValues()
		{
			List<ProjectItemRequirement> knownRequirements = new List<ProjectItemRequirement>(2);
			Research known = new Research {Key = "local life", Name = "Local Life Forms", IconName = "Antimatter Drill Motor", ResearchCost = 10, PreDescription = "test", PostDescription = "post test", ResearchRequirements = new List<String>(), ProjectRequirements = knownRequirements, ScanRequirements = new List<ScanRequirement>()};
			Research actual = ResearchList["local life"];

			Assert.AreEqual(known.Key, actual.Key, "Keys do not match");
			Assert.AreEqual(known.Name, actual.Name, "Names do not match");
			Assert.AreEqual(known.IconName, actual.IconName, "Icon names do not match");
			Assert.AreEqual(known.ResearchCost, actual.ResearchCost, "Research costs do not match");
			Assert.AreEqual(known.PreDescription, actual.PreDescription, "Pre descriptions do not match");
			Assert.AreEqual(known.PostDescription, actual.PostDescription, "Post descriptions do not match");
			Assert.IsTrue(known.ResearchRequirements.SequenceEqual(actual.ResearchRequirements), "Research requirements do not match");
			Assert.IsTrue(known.ProjectRequirements.SequenceEqual(actual.ProjectRequirements), "Project requirements do not match");
			Assert.IsTrue(known.ScanRequirements.SequenceEqual(actual.ScanRequirements), "Scan requirements do not match");
		}


		[TestMethod]
		public void TestResearchSaving()
        {
            Assert.Fail();
            /*
			String filePath = @"Test Files\Install\Research - Saved.xml";
			Research.SaveToFile(filePath, new List<Research>(ResearchList.Values));
			IDictionary<String, Research> reloaded = Research.LoadFromFile(filePath);
			foreach (Research research in ResearchList.Values)
			{
				Research known = research;
				Research actual = reloaded[research.Key];

				Assert.AreEqual(known.Key, actual.Key, "Keys do not match");
				Assert.AreEqual(known.Name, actual.Name, "Names do not match");
				Assert.AreEqual(known.IconName, actual.IconName, "Icon names do not match");
				Assert.AreEqual(known.ResearchCost, actual.ResearchCost, "Research costs do not match");
				Assert.AreEqual(known.PreDescription, actual.PreDescription, "Pre descriptions do not match");
				Assert.AreEqual(known.PostDescription, actual.PostDescription, "Post descriptions do not match");
				Assert.IsTrue(known.ResearchRequirements.SequenceEqual(actual.ResearchRequirements), "Research requirements do not match");
				Assert.IsTrue(known.ProjectRequirements.SequenceEqual(actual.ProjectRequirements), "Project requirements do not match");
				Assert.IsTrue(known.ScanRequirements.SequenceEqual(actual.ScanRequirements), "Scan requirements do not match");
			}

			foreach (Research research in reloaded.Values)
			{
				Research known = research;
				Research actual = ResearchList[research.Key];

				Assert.AreEqual(known.Key, actual.Key, "Keys do not match");
				Assert.AreEqual(known.Name, actual.Name, "Names do not match");
				Assert.AreEqual(known.IconName, actual.IconName, "Icon names do not match");
				Assert.AreEqual(known.ResearchCost, actual.ResearchCost, "Research costs do not match");
				Assert.AreEqual(known.PreDescription, actual.PreDescription, "Pre descriptions do not match");
				Assert.AreEqual(known.PostDescription, actual.PostDescription, "Post descriptions do not match");
				Assert.IsTrue(known.ResearchRequirements.SequenceEqual(actual.ResearchRequirements), "Research requirements do not match");
				Assert.IsTrue(known.ProjectRequirements.SequenceEqual(actual.ProjectRequirements), "Project requirements do not match");
				Assert.IsTrue(known.ScanRequirements.SequenceEqual(actual.ScanRequirements), "Scan requirements do not match");
			}
            */
		}
	}
}
