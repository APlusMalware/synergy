﻿using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Synergy.Installation;
using Synergy.Xml;

namespace Synergy_Test
{
    /// <summary>
    /// Summary description for XmlDataTests
    /// </summary>
    [TestClass]
    public class XmlDataTests
    {
        [TestMethod]
        public void ReadCubeTypes()
        {
            XmlData xml = new XmlData(@"Test Files\Install\TerrainData.xml");
            List<CubeType> cubeTypes = xml.ReadXmlToType<List<CubeType>>();
        }

        [TestMethod]
        public void ReadResearch()
        {
            XmlData xml = new XmlData(@"Test Files\Install\Research.xml");
            List<Research> researches = xml.ReadXmlToType<List<Research>>();
        }

        [TestMethod]
        public void ReadRecipes()
        {
            XmlData xml = new XmlData(@"Test Files\Install\ManufacturerRecipes.xml");
            List<Recipe> recipes = xml.ReadXmlToType<List<Recipe>>();
        }

        [TestMethod]
        public void ReadItems()
        {
            XmlData xml = new XmlData(@"Test Files\Install\Items.xml");
            List<ItemType> items = xml.ReadXmlToType<List<ItemType>>();
        }
    }
}
